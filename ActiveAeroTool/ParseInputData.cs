﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ActiveAeroTool
{
  static class ParseInputData
  {
    const int SETPT_PARAM_COUNT = 7;
    const int SETPT_PARAM_START_COL = 2;
    const int SETPT_UW_INDEX = 5;
    const int SETPT_RW_INDEX = 6;
    static readonly double[] SETPT_PARAM_TOL = { 1, 1, 0.1, 0.1, 0.1, 0.1, 0.1 };
    static readonly int[] SETPT_PARAM_PRECISION = { 0,0,1,1,1,2,2};
    
    public static void parseInput()
    {
      string path = MainWindow.GetInputDataPath();
      List<string> header = new List<string>();
      List<double?[]> map = new List<double?[]>();
      // Get data
      _getData(path, SETPT_PARAM_PRECISION, out header, out map);

      List<string> offsetHeader = new List<string>();
      List<double?[]> offsetMap = new List<double?[]>();
      _getData(MainWindow.GetOffsetDataPath(), SETPT_PARAM_PRECISION, out offsetHeader, out offsetMap);


      int setpt_count = map.Count;
      int coeff_count = map[0].Length - SETPT_PARAM_COUNT - SETPT_PARAM_START_COL;
      // FRH, RRH, Yaw, Roll, Steer, UW, RW
      // In input these are columns 2,3,4,5,6,7,8 (zero based)
      List<double>[] setpt_params = new List<double>[SETPT_PARAM_COUNT];
      for (int j = 0; j < SETPT_PARAM_COUNT; j++)
      {
        setpt_params[j] = new List<double>();
        for (int i = 0; i < setpt_count; i++)
        {
          bool found = false;
          for (int k = 0; k < setpt_params[j].Count; k++)
          {
            if (Math.Abs(setpt_params[j][k] - map[i][SETPT_PARAM_START_COL + j].Value) < SETPT_PARAM_TOL[j])
            {
              found = true;
              break;
            }
          }
          if (!found) setpt_params[j].Add(map[i][SETPT_PARAM_START_COL + j].Value);
        }
        // Sort list:
        setpt_params[j].Sort();
      }
      // Create map as setpoint index list:
      int[,] setpt_index = new int[setpt_count, SETPT_PARAM_COUNT];
      for (int i = 0; i < setpt_count; i++)
      {
        for (int j = 0; j < SETPT_PARAM_COUNT; j++)
        {
          setpt_index[i, j] = get_index(setpt_params[j], map[i][SETPT_PARAM_START_COL + j].Value, SETPT_PARAM_TOL[j]);
        }
      }

      // Seprate out maps which have a constant UW/RW command:
      // These maps are just a list of setpoint numbers.
      // We make as list of lists, as don't know ahead of itme how many maps, or how big each map is..
      // Map UW/RW config is effectively stored within:
      List<List<int>> submaps = new List<List<int>>();
      List<int> submap = new List<int>();
      // TODO Need to ignore repeated rideheights.  Search backwards to pickup post-run tare.  Or overwrite old with new..?
      for (int i = 0; i < setpt_count; i++)
      {
        bool found = false;
        for (int j = 0; j < submaps.Count; j++)
        {
          if ((setpt_index[i, SETPT_UW_INDEX] == setpt_index[submaps[j][0], SETPT_UW_INDEX]) &&
              (setpt_index[i, SETPT_RW_INDEX] == setpt_index[submaps[j][0], SETPT_RW_INDEX]))
          {
            found = true;
            // Only add if this one isn't already there:
            bool exists = false;
            for (int k = 0; k < submaps[j].Count; k++)
            {
              bool check = true;
              for (int l = 0; l < SETPT_PARAM_COUNT - 2; l++)
              {
                if (setpt_index[i, l] != setpt_index[submaps[j][k], l])
                {
                  check = false;
                  break;
                }
              }
              if (check)
              {
                exists = true;
                break;
              }
            }
            if (!exists) submaps[j].Add(i);
            break;
          }
        }
        if (!found)
        {
          submap = new List<int>();
          submap.Add(i);
          submaps.Add(submap);
        }
      }
      // Create list of UW/RW combinations
      const double min_uw = 0;
      const double max_uw = 1;
      const double step_uw = 0.25;
      const double min_rw = 0;
      const double max_rw = 1;
      const double step_rw = 0.25;
      int n_uw = (int)((max_uw - min_uw) / step_uw) + 1;
      int n_rw = (int)((max_rw - min_rw) / step_rw) + 1;
      double[,,] uw_rw_list = new double[n_uw, n_rw, 2];
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          uw_rw_list[i, j, 0] = min_uw + step_uw * i;
          uw_rw_list[i, j, 1] = min_rw + step_rw * j;
        }
      }
      // For each point get type:
      // 0 = no map available
      // 1 = rw map available
      // 2 = full map available:
      int full_map_threshold = 20;  // min number of points for a full map:
                                    // We store type and submap index, i.e. [uw_index, rw_index, submap_index]
      int[,,] map_types = new int[n_uw, n_rw, 2];
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          map_types[i, j, 0] = 0;
          map_types[i, j, 1] = -1; // i.e. no map..
          // Look for a matching map:
          for (int k = 0; k < submaps.Count; k++)
          {
            if ((Math.Abs(setpt_params[SETPT_UW_INDEX][setpt_index[submaps[k][0], SETPT_UW_INDEX]] - uw_rw_list[i, j, 0]) < SETPT_PARAM_TOL[SETPT_UW_INDEX]) &&
                (Math.Abs(setpt_params[SETPT_RW_INDEX][setpt_index[submaps[k][0], SETPT_RW_INDEX]] - uw_rw_list[i, j, 1]) < SETPT_PARAM_TOL[SETPT_RW_INDEX]))
            {
              map_types[i, j, 0] = (submaps[k].Count > full_map_threshold) ? 2 : 1;
              map_types[i, j, 1] = k;
              break;
            }
          }
        }
      }
      // Build output array:
      const double min_frh = 35;
      const double max_frh = 195;
      const double step_frh = 20;
      int n_frh = (int)((max_frh - min_frh) / step_frh) + 1;

      const double min_rrh = 40;
      const double max_rrh = 200;
      const double step_rrh = 20;
      int n_rrh = (int)((max_rrh - min_rrh) / step_rrh) + 1;

      const double min_yaw = 0;
      const double max_yaw = 8;
      const double step_yaw = 2;
      int n_yaw = (int)((max_yaw - min_yaw) / step_yaw) + 1;

      const double min_roll = 0;
      const double max_roll = 1.5;
      const double step_roll = 0.5;
      int n_roll = (int)((max_roll - min_roll) / step_roll) + 1;

      const double min_steer = 0;
      const double max_steer = 8;
      const double step_steer = 2;
      int n_steer = (int)((max_steer - min_steer) / step_steer) + 1;

      double[,] param_limits = new double[SETPT_PARAM_COUNT, 3];
      param_limits[0, 0] = min_frh;
      param_limits[0, 1] = max_frh;
      param_limits[0, 2] = step_frh;

      param_limits[1, 0] = min_rrh;
      param_limits[1, 1] = max_rrh;
      param_limits[1, 2] = step_rrh;

      param_limits[2, 0] = min_yaw;
      param_limits[2, 1] = max_yaw;
      param_limits[2, 2] = step_yaw;

      param_limits[3, 0] = min_roll;
      param_limits[3, 1] = max_roll;
      param_limits[3, 2] = step_roll;

      param_limits[4, 0] = min_steer;
      param_limits[4, 1] = max_steer;
      param_limits[4, 2] = step_steer;

      param_limits[5, 0] = min_uw;
      param_limits[5, 1] = max_uw;
      param_limits[5, 2] = step_uw;

      param_limits[6, 0] = min_rw;
      param_limits[6, 1] = max_rw;
      param_limits[6, 2] = step_rw;


      // For each rear wing map, extrapolate out over full ride map.
      double[,] rh_map = new double[n_frh * n_rrh, 2];
      int index = 0;
      for (int i = 0; i < n_rrh; i++)
      {
        for (int j = 0; j < n_frh; j++)
        {
          rh_map[index, 0] = min_frh + step_frh * j;
          rh_map[index, 1] = min_rrh + step_rrh * i;
          index++;
        }
      }
      // Get Penske to WSI offset maps:
      double[][,] offsetMaps = getOffsetMaps(rh_map, offsetMap);


      int[] param_count = new int[SETPT_PARAM_COUNT];
      for (int i=0;i< SETPT_PARAM_COUNT;i++)
      {
        param_count[i] = (int)((param_limits[i, 1] - param_limits[i, 0]) / param_limits[i, 2])+1;
      }
      
      int n_out = n_uw * n_rw * n_frh * n_rrh * n_yaw * n_roll * n_steer;
      Console.WriteLine($"Number of points = {n_out}");

      // Create map of frh/rrh/roll/steer/yaw values:
      int n_ride_map = n_frh * n_rrh * n_yaw * n_roll * n_steer;
      double[,] ride_map = new double[n_ride_map, 5];
      index = 0;
      for (int i = 0; i < n_steer; i++)
      {
        for (int j = 0; j < n_roll; j++)
        {
          for (int k = 0; k < n_yaw; k++)
          {
            for (int l = 0; l < n_rrh; l++)
            {
              for (int m = 0; m < n_frh; m++)
              {
                ride_map[index, 0] = min_frh + step_frh * m;
                ride_map[index, 1] = min_rrh + step_rrh * l;
                ride_map[index, 2] = min_yaw + step_yaw * k;
                ride_map[index, 3] = min_roll + step_roll * j;
                ride_map[index, 4] = min_steer + step_steer * i;
                index++;
              }
            }
          }
        }
      }
      // Initialise output data:
      double[,][,] out_maps = new double[n_uw, n_rw][,];

      // Create full map based maps:
      // Firstly get list of uw/rw points having full maps, and index of that map:
      List<int[]> full_maps = new List<int[]>();
      // For each map we store submap index, uw index, rw index
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          if (map_types[i, j, 0] == 2)
          {
            int[] full_map = new int[3];
            full_map[0] = i;
            full_map[1] = j;
            full_map[2] = map_types[i, j, 1];
            full_maps.Add(full_map);
          }
        }
      }
      // Interpolate maps:
      AeroTool.AeroModelInterface AMI = new AeroTool.AeroModelInterface();
      double[] ride_tol = new double[SETPT_PARAM_COUNT - 2];
      for (int i = 0; i < SETPT_PARAM_COUNT - 2; i++) ride_tol[i] = SETPT_PARAM_TOL[i];
      for (int i = 0; i < full_maps.Count; i++)
      {
        // Build intput data:
        double[,] map_data_in = new double[submaps[full_maps[i][2]].Count, SETPT_PARAM_COUNT + coeff_count - 2];
        for (int j = 0; j < submaps[full_maps[i][2]].Count; j++)
        {
          for (int k = 0; k < SETPT_PARAM_COUNT - 2; k++)
          {
            map_data_in[j, k] = map[submaps[full_maps[i][2]][j]][SETPT_PARAM_START_COL + k].Value;
          }
          for (int k = 0; k < coeff_count; k++)
          {
            map_data_in[j, k + SETPT_PARAM_COUNT - 2] = map[submaps[full_maps[i][2]][j]][SETPT_PARAM_START_COL + SETPT_PARAM_COUNT + k].Value;
          }
        }
        
        MainWindow.ShowMessage($"Interpolating full map {i} of {full_maps.Count}");
        double[,] interp_data = AMI.InterpolateMap(ref map_data_in, ref ride_tol, ref ride_map);
        out_maps[full_maps[i][0], full_maps[i][1]] = interp_data;
      }
      MainWindow.ShowMessage($"Done interpolating full maps.");
      // List of RW maps:
      List<int[]> rw_maps = new List<int[]>();
      // For each map we store submap index, uw index, rw index
      bool rw_ride_map_set = false;
      double[] rw_map_yaw_angles = new double[2];
      rw_map_yaw_angles[0] = 0;
      rw_map_yaw_angles[1] = 4;
      int n_rw_ride_map = 0;
      double[,] rw_map_data_in = new double[n_rw_ride_map, 2 + coeff_count];
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          if (map_types[i, j, 0] > 0)
          {
            int[] rw_map = new int[3];
            rw_map[0] = i;
            rw_map[1] = j;
            rw_map[2] = map_types[i, j, 1];
            rw_maps.Add(rw_map);
            // If this is first rw ride map that we have come across, get map:
            if (!rw_ride_map_set && (map_types[i, j, 0] == 1))
            {
              // Count number of points:
              for (int k = 0; k < submaps[map_types[i, j, 1]].Count; k++)
              {
                if (map[submaps[rw_map[2]][k]][4] == rw_map_yaw_angles[0]) n_rw_ride_map++;
              }
              rw_map_data_in = new double[n_rw_ride_map, 2 + coeff_count];
              for (int k = 0; k < submaps[map_types[i, j, 1]].Count; k++)
              {
                if (map[submaps[rw_map[2]][k]][4] == rw_map_yaw_angles[0])
                {
                  rw_map_data_in[k, 0] = map[submaps[rw_map[2]][k]][2].Value;
                  rw_map_data_in[k, 1] = map[submaps[rw_map[2]][k]][3].Value;
                }
              }
              rw_ride_map_set = true;
            }
          }
        }
      }
      // RW maps - ride maps at 0 and 4° yaw:
      double[,,][,] rw_map_data = new double[n_uw, n_rw, 2][,];
      bool[,,] rw_map_done = new bool[n_uw, n_rw, 2];
      double[] tol = new double[2];
      tol[0] = SETPT_PARAM_TOL[0];
      tol[1] = SETPT_PARAM_TOL[1];
      // Extrapolate RH maps for existing RW maps:
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          for (int k = 0; k < 2; k++) rw_map_done[i, j, k] = false;
          if (map_types[i, j, 0] > 0) // i.e. full map or RW map
          {
            // Collect data:
            for (int m = 0; m < 2; m++) // 0 and 4° yaw angle.
            {
              for (int l = 0; l < n_rw_ride_map; l++)
              {
                for (int k = 0; k < submaps[map_types[i, j, 1]].Count; k++)
                {
                  if ((Math.Abs(map[submaps[map_types[i, j, 1]][k]][2].Value - rw_map_data_in[l, 0]) < tol[0]) &&
                    (Math.Abs(map[submaps[map_types[i, j, 1]][k]][3].Value - rw_map_data_in[l, 1]) < tol[1]) &&
                    (Math.Abs(map[submaps[map_types[i, j, 1]][k]][4].Value - rw_map_yaw_angles[m]) < SETPT_PARAM_TOL[2]) &&
                    (Math.Abs(map[submaps[map_types[i, j, 1]][k]][5].Value) < SETPT_PARAM_TOL[3]) &&
                    (Math.Abs(map[submaps[map_types[i, j, 1]][k]][6].Value) < SETPT_PARAM_TOL[4]))
                  {
                    for (int n = 0; n < coeff_count; n++)
                    {
                      rw_map_data_in[l, n + 2] = map[submaps[map_types[i, j, 1]][k]][2 + SETPT_PARAM_COUNT + n].Value;
                    }
                    // Found setpoint so exit:
                    break;
                  }
                }
              }
              // Interpolate:
              MainWindow.ShowMessage($"Building rh maps for RW{i*n_rw+j} of {n_rw*n_uw}");
              rw_map_data[i, j, m] = interp_2d(rw_map_data_in, rh_map, 10f);
              rw_map_done[i, j, m] = true;
            }
          }
        }
      }
      // Extrapolate remaining maps:
      List<int[]> missing_rw_maps = new List<int[]>();
      List<int[]> existing_rw_maps = new List<int[]>();
      int[] rw_map_id = new int[2];
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          rw_map_id = new int[2];
          rw_map_id[0] = i;
          rw_map_id[1] = j;
          if (!rw_map_done[i, j,0]) missing_rw_maps.Add(rw_map_id);
          else existing_rw_maps.Add(rw_map_id);
        }
      }
      // Interpolate for each rideheight point:
      // Build input map
      double[,] rw_map_in = new double[existing_rw_maps.Count, 2+coeff_count];
      for (int i = 0; i < existing_rw_maps.Count; i++)
      {
        rw_map_in[i, 0] = min_uw + existing_rw_maps[i][0] * step_uw;
        rw_map_in[i, 1] = min_rw + existing_rw_maps[i][1] * step_rw;
      }
      // Create list of points to create:
      double[,] rw_map_out = new double[missing_rw_maps.Count, 2];
      for (int i = 0; i < missing_rw_maps.Count; i++)
      {
        rw_map_out[i, 0] = min_uw + missing_rw_maps[i][0] * step_uw;
        rw_map_out[i, 1] = min_rw + missing_rw_maps[i][1] * step_rw;
      }
      // Array of interpolated data:
      double[,][,] rw_interp_data = new double[n_frh * n_rrh, 2][,];
      for (int i = 0; i < n_frh * n_rrh; i++)
      {
        // And at each RW angle:
        for (int j = 0; j < 2; j++)
        {
          // For each coefficient:
          for (int k=0;k<coeff_count;k++)
          {
            for (int l=0;l<existing_rw_maps.Count;l++)
            {
              rw_map_in[l, 2+k] = rw_map_data[existing_rw_maps[l][0], existing_rw_maps[l][1], j][i,k];
            }
          }
          // Interpolate data:
          MainWindow.ShowMessage($"Interpolating uw/rw maps for rideheight{i} of {n_frh*n_rrh}, rw angle {j}/2");
          rw_interp_data[i, j] = interp_2d(rw_map_in, rw_map_out, 10f);
        }
      }
      // Construct remaining rear wing maps:
      for (int i = 0; i < missing_rw_maps.Count; i++)
      {
        for (int j = 0; j < 2; j++)
        {
          double[,] rw_map = new double[n_frh * n_rrh, coeff_count];
          for (int k = 0; k < n_frh * n_rrh; k++)
          {
            for (int l = 0; l < coeff_count; l++)
            {
              rw_map[k, l] = rw_interp_data[k, j][i, l];
            }
          }
          rw_map_data[missing_rw_maps[i][0], missing_rw_maps[i][1], j] = rw_map;
        }
      }
      // Right-ho, so now we can use these maps to offset the baseline data.
      // For each UW/RW combo, identify base map, then offset accordingly.
      MainWindow.ShowMessage($"Putting all data together.");
      int base_map_index = 0;

      for (int i=0;i<n_uw;i++)
      {
        double uw = min_uw + step_uw * i;
        for (int j = 0; j < n_rw; j++)
        {
          // Only need do if not a full map already:
          if (map_types[i, j, 0] == 2) continue;
          // Find nearest full map point:
          double min_dist = 100;
          double rw = min_rw + step_rw * j;

          for (int k = 0; k < full_maps.Count; k++)
          {
            double base_uw = min_uw + full_maps[k][0] * step_uw;
            double base_rw = min_rw + full_maps[k][1] * step_rw;
            double dist = Math.Sqrt(Math.Pow(uw - base_uw, 2) + 0.001 * Math.Pow(rw - base_rw, 2));
            if (dist < min_dist)
            {
              min_dist = dist;
              base_map_index = k;
            }
          }
          MainWindow.ShowMessage($" uw[{i},{j}] index = {base_map_index}.");
          double[,] rh_data = (double[,])out_maps[full_maps[base_map_index][0], full_maps[base_map_index][1]].Clone();
          // New map is base map plus delta rw map.
          for (int k = 0; k < n_steer; k++)
          {
            for (int l = 0; l < n_roll; l++)
            {
              for (int m = 0; m < n_yaw; m++)
              {
                for (int n = 0; n < n_rrh; n++)
                {
                  for (int o = 0; o < n_frh; o++)
                  {
                    int full_map_index = o + n * n_frh + m * n_frh * n_rrh + l * n_yaw * n_rrh * n_frh + k * n_frh * n_rrh * n_yaw * n_roll;
                    int rh_index = o + n * n_frh;
                    double yaw_angle = min_yaw + m * step_yaw;
                    // Interpolate:
                    for (int p = 0; p < coeff_count; p++)
                    {
                      double delta4 = rw_map_data[i, j, 1][rh_index, p] - rw_map_data[full_maps[base_map_index][0], full_maps[base_map_index][0], 1][rh_index, p];
                      if (yaw_angle <= 4)
                      {
                        double delta0 = rw_map_data[i, j, 0][rh_index, p] - rw_map_data[full_maps[base_map_index][0], full_maps[base_map_index][1], 0][rh_index, p];
                        rh_data[full_map_index, p] += delta0 + (delta4 - delta0) * yaw_angle / 4;
                      }
                      else
                      {
                        rh_data[full_map_index, p] += delta4;
                      }
                    }
                  }
                }
              }
            }
          }
          out_maps[i, j] = rh_data;
        }
      }
      // Need to add WSI to PTG offsets to all data:
      for (int i = 0; i < n_uw; i++)
      {
        for (int j = 0; j < n_rw; j++)
        {
          for (int k = 0; k < n_steer; k++)
          {
            for (int l = 0; l < n_roll; l++)
            {
              for (int m = 0; m < n_yaw; m++)
              {
                double yaw_angle = min_yaw + m * step_yaw;
                for (int n = 0; n < n_rrh; n++)
                {
                  for (int o = 0; o < n_frh; o++)
                  {
                    int full_map_index = o + n * n_frh + m * n_frh * n_rrh + l * n_yaw * n_rrh * n_frh + k * n_frh * n_rrh * n_yaw * n_roll;
                    int rh_index = o + n * n_frh;
                    for (int p = 0; p < coeff_count; p++)
                    {
                      // Add Penske to WSI delta:
                      double delta4 = offsetMaps[1][rh_index, p];
                      if (yaw_angle <= 4)
                      {
                        double delta0 = offsetMaps[0][rh_index, p];
                        out_maps[i,j][full_map_index, p] += delta0 + (delta4 - delta0) * yaw_angle / 4;
                      }
                      else
                      {
                        out_maps[i, j][full_map_index, p] += delta4;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      MainWindow.ShowMessage($"Writing file out.");
      writeAEMFile(MainWindow.GetOutputDataPath(), path,out_maps, header, coeff_count, param_limits,param_count, ride_map, rw_map_out);
      MainWindow.ShowMessage($"Done.");
    }

    // Get offset maps.  For now we assume they are only at 0 and 4° yaw.
    public static double[][,] getOffsetMaps(double[,] rh_map, List<double?[]> data_in)
    {
      int coeff_count = data_in[0].Length - SETPT_PARAM_COUNT - SETPT_PARAM_START_COL;
      double[][,] ret = new double[2][,];
      // Assemble maps:
      List<int>[] maps = new List<int>[2];
      maps[0] = new List<int>();
      maps[1] = new List<int>();
      for (int i=0;i<data_in.Count;i++)
      {
        int type = -1;
        if (Math.Abs(data_in[i][SETPT_PARAM_START_COL+2].Value - 0) < SETPT_PARAM_TOL[2]) type = 0;

        else if (Math.Abs(data_in[i][SETPT_PARAM_START_COL + 2].Value - 4) < SETPT_PARAM_TOL[2]) type = 1;
        else continue;

        // See if point already exists:
        bool found = false;
        for (int j=0;j<maps[type].Count;j++)
        {
          found = true;
          for (int k=0;k<SETPT_PARAM_COUNT;k++)
          {
            if (Math.Abs(data_in[maps[type][j]][k].Value - data_in[i][k].Value) > SETPT_PARAM_TOL[k])
            {
              found = false;
              break;
            }
          }
          if (found)
          {
            // Replace setpoint:
            maps[type][j] = i;
            break;
          }
        }
        if (!found)
        {
          // Add setpoint:
          maps[type].Add(i);
        }
      }
      // Put data into arrays:
      double[,] map_data_in;
      for (int i=0;i<2;i++)
      {
        // Return list of just rideheights and data
        map_data_in = new double[maps[i].Count, coeff_count + 2];
        for (int j=0;j<maps[i].Count;j++)
        {
          map_data_in[j,0] = data_in[maps[i][j]][SETPT_PARAM_START_COL].Value;
          map_data_in[j, 1] = data_in[maps[i][j]][SETPT_PARAM_START_COL+1].Value;
          for (int k = 0; k < coeff_count; k++)
          {
            map_data_in[j, 2+k] = data_in[maps[i][j]][SETPT_PARAM_START_COL+SETPT_PARAM_COUNT+k].Value;
          }
        }
        // Interpolate:
        ret[i] = interp_2d(map_data_in, rh_map, 10.0f);
      }
      return ret;
    }



    public static void writeAEMFile(string output_path, string in_file, double[,][,] rh_data, List<String> raw_header, int coeff_count, double[,] param_limits, int[] param_count, double[,] ride_map, double[,] rw_map)
    {
      StreamWriter sw = new StreamWriter(output_path);
      sw.WriteLine($"!AEM file created {DateTime.Now.ToString("HH:mm dd/MM/yyyy")} by {Environment.UserName}");
      sw.WriteLine($"!Data taken from file {in_file}");
      sw.WriteLine("!X= front ride height (mm)");
      sw.WriteLine("!Y= rear ride height (mm)");
      sw.WriteLine();

      sw.WriteLine("Constants");
      // No constants for now.
      sw.WriteLine();
      // Write available outputs
      sw.WriteLine("Available outputs");
      for (int i = 0; i < coeff_count; i++)
      {
        sw.WriteLine($"\t{raw_header[i + 2+param_count.Length]}");
      }
      sw.WriteLine("");
      sw.WriteLine("Available Inputs (name, required, action, default value, symetrical, numvals, extrap)");
      sw.WriteLine($"\tFRH,true,stop,2,false,{param_count[0]}, none");
      sw.WriteLine($"\tRRH,true,ignore,2,false,{param_count[1]}, none");
      sw.WriteLine($"\t{raw_header[7]},false,warn,0,True,{param_count[5]},none");
      sw.WriteLine($"\t{raw_header[8]},false,warn,0,True,{param_count[6]},none");
      sw.WriteLine($"\t{raw_header[4]},false,warn,0,True,{param_count[2]},none");
      sw.WriteLine($"\t{raw_header[5]},false,warn,0,True,{param_count[3]},none");
      sw.WriteLine($"\t{raw_header[6]},false,warn,0,True,{param_count[4]},none");
      sw.WriteLine("");
      sw.WriteLine("!MAP DATA");
      string str = $"{raw_header[2]},{raw_header[3]},{raw_header[7]},{raw_header[8]},{raw_header[4]},{raw_header[5]},{raw_header[6]}";
      for (int i=0;i<coeff_count;i++)
      {
        str += $",{raw_header[i + param_count.Length + 2]}";
      }
      sw.WriteLine(str);

      // Write data:
      for (int i = 0; i < param_count[4]; i++)
      {
        for (int j = 0; j < param_count[3]; j++)
        {
          for (int k = 0; k < param_count[2]; k++)
          {
            for (int rw = 0; rw < param_count[5]; rw++)
            {
              for (int uw = 0; uw < param_count[6]; uw++)
              {
                for (int l = 0; l < param_count[1]; l++)
                {
                  for (int m = 0; m < param_count[0]; m++)
                  {
                    int index = m + l * param_count[0] +
                              k * param_count[0] * param_count[1] +
                              j * param_count[0] * param_count[1] * param_count[2] +
                              i * param_count[0] * param_count[1] * param_count[2] * param_count[3];
                    // Write rideheights:
                    str = ride_map[index, 0].ToString("F2");
                    str += $",{ride_map[index, 1].ToString("F2")}";
                    // UW/RW
                    str += $",{(param_limits[5, 0] + param_limits[5, 2] * uw).ToString("F2")}";
                    str += $",{(param_limits[6, 0] + param_limits[6, 2] * rw).ToString("F2")}";
                    // Yaw/roll/steer
                    for (int n=0; n<3;n++) str+= $",{ride_map[index, n+2].ToString("F2")}";
                    // Coeffs
                    for (int n=0;n<coeff_count;n++) str+= $",{rh_data[uw,rw][index, n].ToString("F6")}";
                    sw.WriteLine(str);
                  }
                }
              }
            }
          }
        }
      }





      //for (int i = 0; i < param_count[5]; i++)
      //{
      //  for (int j = 0; j < param_count[6]; j++)
      //  {
      //    double[,] data = rh_data[i, j];
      //    for (int k = 0; k < data.GetLength(0); k++)
      //    {
      //      str = ride_map[k, 0].ToString("F2");
      //      for (int l = 1; l < 5; l++)
      //      {
      //        str += $",{ride_map[k, l].ToString("F2")}";
      //      }
      //      str += $",{(param_limits[5, 0] + param_limits[5, 2] * i).ToString("F2")}";
      //      str += $",{(param_limits[6, 0] + param_limits[6, 2] * j).ToString("F2")}";
      //      for (int l=0;l<coeff_count;l++)
      //      {
      //        str += $",{data[k, l].ToString("F6")}";
      //      }
      //      sw.WriteLine(str);
      //    }
      //  }
      //}

      sw.Close();

    }

    private static double[,] interp_2d(double[,] data_in, double[,] rh_map, float ARMax)
    {
      double[,] ret;
      int n_in = data_in.GetLength(0);
      int n_out = rh_map.GetLength(0);
      int n_coeffs = data_in.GetLength(1) - 2;
      ret = new double[n_out, n_coeffs];
      AeroTool.Interpolate ati = new AeroTool.Interpolate();
      double[,] input = new double[n_in, 3];
      for (int i = 0; i < n_in; i++)
      {
        for (int j = 0; j < 2; j++)
        {
          input[i, j] = data_in[i, j];
        }
      }
      for (int i = 0; i < n_coeffs; i++)
      {
        for (int j = 0; j < n_in; j++)
        {
          input[j, 2] = data_in[j, 2 + i];

        }
        double[] interp_data = ati.InterpVals(input, rh_map, ARMax);
        for (int j = 0; j < n_out; j++)
        {
          ret[j, i] = interp_data[j];
        }
      }
      return ret;
    }

    private static int get_index(List<double> list, double value, double tolerance)
    {
      int ret = -1;
      for (int i=0;i<list.Count;i++)
      {
        if (Math.Abs(list[i] - value) <= tolerance)
        {
          ret = i;
          break;
        }
      }
      return ret;
    }

    // Collects setpoint info.
    private static void _getData(string path, int[] precision, out List<string> header, out List<double?[]> map)
    {
      header = new List<string>();
      map = new List<double?[]>();
      try
      {
        using (var reader = new StreamReader(path))
        {
          var line = reader.ReadLine();
          string[] values = line.Split(',');

          int colCount = 0;
          for (int i=0;i<values.Length;i++)
          {
            if (values[i].Length > 0)
            {
              colCount++;
            }
            else break;
          }
          double?[] setpoint = new double?[colCount];
          for (int i = 0; i < colCount; i++)
          {
            header.Add(values[i]);
          }
          while (!reader.EndOfStream)
          {
            line = reader.ReadLine();
            values = line.Split(',');
            double val;
            if (double.TryParse(values[0], out val))
            {
              setpoint = new double?[colCount];
              setpoint[0] = val;
              for (int i = 1; i < colCount; i++)
              {
                if (double.TryParse(values[i], out val))
                {
                  // Make all params positive:
                  setpoint[i] = val;
                  if ((i >= 2) && (i < (SETPT_PARAM_COUNT + 2)))
                  {
                    setpoint[i] = Math.Abs(setpoint[i].Value);
                    setpoint[i] = Math.Round(setpoint[i].Value, precision[i-2]);
                  }
                }
                else
                {
                  setpoint[i] = null;
                }
              }
              map.Add(setpoint);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
    }

  }
}
