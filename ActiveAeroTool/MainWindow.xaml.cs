﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ActiveAeroTool
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    #region Members
    private static MainWindow frm;
    #endregion //Members


    public MainWindow()
    {
      InitializeComponent();
      frm = this;
    }
    #region Event Handlers
    private void cmdOpenData_Click(object sender, RoutedEventArgs e)
    {
      Task t = new Task(()=>ParseInputData.parseInput());
      t.Start();
    }
    #endregion // Event Handlers

    #region Dispatchers
    public static string GetInputDataPath()
    {
      string path = "";
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(() =>
        {
          path = frm.txtPath.Text;
        }));
      }
      else
      {
        path = frm.txtPath.Text;
      }
      return path;
    }

    public static string GetOffsetDataPath()
    {
      string path = "";
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(() =>
        {
          path = frm.txtOffsetPath.Text;
        }));
      }
      else
      {
        path = frm.txtOffsetPath.Text;
      }
      return path;
    }

    public static string GetOutputDataPath()
    {
      string path = "";
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(() =>
        {
          path = frm.txtOutputPath.Text;
        }));
      }
      else
      {
        path = frm.txtOutputPath.Text;
      }
      return path;
    }

    public static void ShowMessage(string message)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<string>(ShowMessage), new object[] { message });
        return;
      }
      frm.txtInfo.AppendText(message + "\r\n");
      frm.txtInfo.ScrollToEnd();
      frm.UpdateLayout();
      
    }
    #endregion // Dispatchers
  }
}
