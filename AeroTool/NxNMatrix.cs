using System;
using System.Collections.Generic;
using System.Text;
namespace AeroTool
{
    public class NxNMatrix
    {
        public double[,] Elements;
        public int NSize;
        public bool flError;
        public NxNMatrix(int N)
        //Constructor
        {
            Elements = new double[N, N];
            NSize=N;
            flError=false;
            //Set all values to zero:
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Elements[i, j] = 0.0;
                }
            }
        }

        public double Determinant()
        //Calculate determinant by gaussian elimination then product of diagonal:
        {
            double[,] GaussMat = (double[,])Elements.Clone();
            double dblDeterm = 0;
            bool flSwap = false;
            double dblSmall = 1e-6;
            //If N=3, then calculate directly:
            if (NSize==3)
            {
                dblDeterm = GaussMat[0, 0] * (GaussMat[1, 1] * GaussMat[2, 2] - GaussMat[1, 2] * GaussMat[2, 1]) +
                            GaussMat[0, 1] * (GaussMat[1, 2] * GaussMat[2, 0] - GaussMat[1, 0] * GaussMat[2, 2]) +
                            GaussMat[0, 2] * (GaussMat[1, 0] * GaussMat[2, 1] - GaussMat[1, 1] * GaussMat[2, 0]);
                return dblDeterm;
            }
            for (int i = 0; i < NSize - 1; i++)
            {
                //Swap rows so that we have row with greatest value in i-1th column
                //in ith row:
                int iRow = i;
                for (int j = i + 1; j < NSize; j++)
                {
                    if (Math.Abs(GaussMat[j, i]) > Math.Abs(GaussMat[iRow, i]))
                    {
                        iRow = j;
                    }
                }
                if (iRow != i)
                //Swap rows:
                {
                    flSwap = !flSwap;
                    double dblTemp;
                    for (int j = 0; j < NSize; j++)
                    {
                        dblTemp = GaussMat[i, j];
                        GaussMat[i, j] = GaussMat[iRow, j];
                        GaussMat[iRow, j] = dblTemp;
                    }
                }
                //If diagonal element is zero, system insolvable and determinant is zero.
                if (Math.Abs(GaussMat[i, i]) <dblSmall)
                {
                    return 0;
                }
                //Perform elimination on ith column:
                for (int j = i + 1; j < NSize; j++)
                {
                    double dblFact = GaussMat[j, i] / GaussMat[i, i];
                    for (int k = i; k < NSize; k++)
                    {
                        GaussMat[j, k] = GaussMat[j,k] - GaussMat[i,k]* dblFact;
                    }
                }
            }
            //Calculate determinant by multiplication of diagonal elements:
           dblDeterm = GaussMat[0, 0];
            for (int i = 1; i < NSize; i++)
            {
                dblDeterm *= GaussMat[i, i];
            }
            if (flSwap)
            {
                dblDeterm *= -1.0f;
            }
            return dblDeterm;
        }

        public NxNMatrix Inverse()
        //Calculates inverse of matrix by gauss-jordan elimination.
        {
            NxNMatrix InvMat = new NxNMatrix(NSize);
            double[,] dblInput = (double[,])Elements.Clone();
            double[,] dblInv = InvMat.Elements;
            //Create identity matrix:
            for (int i = 0; i < NSize; i++)
            {
                for (int j = 0; j < NSize; j++)
                {
                    if (i == j)
                    {
                        dblInv[i,j] = 1.0f;
                    }
                    else
                    {
                        dblInv[i,j] = 0.0f;
                    }
                }
            }
            //Perform gaussian elimination:
            for (int i = 0; i < NSize - 1; i++)
            {
                //Swap rows so that we have row with greatest absolute value in i-1th column
                //in ith row:
                int iRow = i;
                for (int j = i + 1; j < NSize; j++)
                {
                    if (Math.Abs(dblInput[j, i]) > Math.Abs(dblInput[i, i]))
                    {
                        iRow = j;
                    }
                }
                if (iRow != i)
                //Swap rows:
                {
                    double dblTemp;
                    for (int j = 0; j < NSize; j++)
                    {
                        dblTemp = dblInput[i, j];
                        dblInput[i, j] = dblInput[iRow, j];
                        dblInput[iRow, j] = dblTemp;
                        dblTemp = dblInv[i, j];
                        dblInv[i, j] = dblInv[iRow, j];
                        dblInv[iRow, j] = dblTemp;
                    }
                }
                //If diagonal element is zero, system insolvable and determinant is zero.
                if (dblInput[i, i] == 0)
                {
                    InvMat.flError = true;
                    return InvMat;
                }
                //Perform elimination on ith column:
                for (int j = i + 1; j < NSize; j++)
                {
                    double dblFact = dblInput[j, i] / dblInput[i, i];
                    for (int k = 0; k < NSize; k++)
                    {
                        dblInput[j, k] = dblInput[j, k] - dblInput[i, k] * dblFact;
                        dblInv[j, k] = dblInv[j, k] - dblInv[i, k] * dblFact;
                    }
                }
            }
            //Backwards substitution:
            for (int i = NSize - 1; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    double dblFact = dblInput[j, i] / dblInput[i, i];
                    for (int k = 0; k < NSize; k++)
                    {
                        dblInput[j, k] = dblInput[j, k] - dblInput[i, k] * dblFact;
                        dblInv[j, k] = dblInv[j, k] - dblInv[i, k] * dblFact;
                    }
                }
            }
            //Reduce diagonal elements to 1:
            for (int i = 0; i < NSize; i++)
            {
                double dblFact = 1.0f / dblInput[i, i];
                dblInput[i, i] *= dblFact;
                for (int j = 0; j < NSize; j++)
                {
                    dblInv[i, j] *= dblFact;
                }
            }
            InvMat.Elements = (double[,])dblInv.Clone();
            return InvMat;
        }
    }
}
