﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace AeroTool
{
    public class Logger
    {
        public Stopwatch sw= new Stopwatch();
        public string strPath = "";
        public  void CreateLog()
        {
            sw.Reset();
            sw.Start();
            //Get applicationdata folder path.
            strPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)+ "\\AeroMotive";
            if (!Directory.Exists(strPath))
            {
                Directory.CreateDirectory(strPath);
            }
            strPath = strPath + "\\AeroToolLog.txt";
            using (StreamWriter w = File.CreateText(strPath))
            {
                w.WriteLine("Contour log created " + DateTime.Now.ToLongTimeString());
                //w.WriteLine("IS high frequency?" + Stopwatch.IsHighResolution.ToString());
                //w.WriteLine("SW Frequency " + Stopwatch.Frequency.ToString());
            }
        }


        public void Log(string logMessage)
        {
            using (StreamWriter w = File.AppendText(strPath))
            {
                w.Write("Log Entry : ");
                w.WriteLine("{0}:{1}", sw.ElapsedMilliseconds + "ms", logMessage);
                // Update the underlying file.
                w.Flush();
            }
        }
        public void StopTimer()
        {
            sw.Stop();
        }

    }
}
