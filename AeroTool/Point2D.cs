using System;
using System.Collections.Generic;
using System.Text;

namespace AeroTool
{
    public class Point2D
    {
        public double X;
        public double Y;
        public double Value;
        public bool flTriangulated;
        public bool flError;

        public Point2D()
        //Constructor
        {
            X = 0;
            Y = 0;
            Value = 0;
            flTriangulated = false;
            flError = false;
        }
        public Point2D Copy()
        {
            Point2D PtCopy = new Point2D();
            PtCopy.flTriangulated=flTriangulated;
            PtCopy.Value=Value;
            PtCopy.X=X;
            PtCopy.Y=Y;
            PtCopy.flError = flError;
            return PtCopy;
        }
    }
}
