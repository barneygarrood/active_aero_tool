using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AeroTool

{
    public interface IInterpolateMap
    {
        double[] InterpVals(ref double[,] dblPtList, ref double[,] dblQueryPts, ref float ARMax);
    }
    //Make class COM visible:
    [ComVisible(true)]
    //Force to use an explicitly defined interface:
    [ClassInterface(ClassInterfaceType.None)]
    public class Interpolate
    {
        public double[] InterpVals(double[,] dblPtList, double[,] dblQueryPts, float ARMax)
        {
            int NOutPts = dblQueryPts.GetLength(0);
            bool[] flOutsideHull = new bool[NOutPts];
            //Note that by default we extrapolate (last parameter is true)
            double[] dblOutput = InterpVals(dblPtList, dblQueryPts, ARMax, ref flOutsideHull, true);
            return dblOutput;
        }

        public double[] InterpValsExtrapOption(double[,] dblPtList, double[,] dblQueryPts, float ARMax, bool flExtrapolate)
        {
            int NOutPts = dblQueryPts.GetLength(0);
            bool[] flOutsideHull = new bool[NOutPts];
            double[] dblOutput = InterpVals(dblPtList, dblQueryPts, ARMax, ref flOutsideHull, flExtrapolate);
            return dblOutput;
        }


        public double[] InterpVals(double[,] dblPtList, double[,] dblQueryPts, float ARMax, ref bool[] flOutsideHull, bool flExtrapolate)
        // Subroutine takes a list of double X, Y, and Z values in dblPtList, and interpolates/extrapolates Z values at the 
        // XY coordinates stored in dblQueryPts.  Inputs all in double format for use direct from Excel.
        {
            int NPts = dblPtList.GetLength(0);
            Point2D[] PtList = new Point2D[NPts];
            for (int i = 0; i < NPts; i++)
            {
                PtList[i] = new Point2D();
                PtList[i].X = dblPtList[i, 0];
                PtList[i].Y = dblPtList[i, 1];
                PtList[i].Value = dblPtList[i, 2];
            }
            double[] dblOutput = InterpVals2DPtInput(PtList, dblQueryPts, ARMax, ref flOutsideHull, flExtrapolate);
            return dblOutput;
        }

        [ComVisible(true)]
        public double[] InterpVals2(ref double[,] dblPtList, ref double[,] dblQueryPts, ref float ARMax, ref bool[] flOutsideHull)
        // Subroutine takes a list of double X, Y, and Z values in dblPtList, and interpolates/extrapolates Z values at the 
        // XY coordinates stored in dblQueryPts.  Inputs all in double format for use direct from Excel.
        //Same as overloaded version above, but COM visible.
        {
            int NPts = dblPtList.GetLength(0);
            Point2D[] PtList = new Point2D[NPts];
            for (int i = 0; i < NPts; i++)
            {
                PtList[i] = new Point2D();
                PtList[i].X = dblPtList[i, 0];
                PtList[i].Y = dblPtList[i, 1];
                PtList[i].Value = dblPtList[i, 2];
            }

            double[] dblOutput = InterpVals2DPtInput(PtList, dblQueryPts, ARMax);
            return dblOutput;
        }

        public double[] InterpVals2DPtInput(Point2D[] PtList, double[,] dblQueryPts, float ARMax)
        {
            int NOutPts = dblQueryPts.GetLength(0);
            bool[] flOutsideHull = new bool[NOutPts];
            double[] dblOutput = InterpVals2DPtInput(PtList, dblQueryPts, ARMax, ref flOutsideHull, true);
            return dblOutput;
        }
        

        public double[] InterpVals2DPtInput(Point2D[] PtList, double[,] dblQueryPts, float ARMax, ref bool[] flOutsideHull, bool flExtrapolate)
        // Subroutine takes a list of double X, Y, and Z values in dblPtList, and interpolates/extrapolates Z values at the 
        // XY coordinates stored in dblQueryPts.  Input is in Point2D format.
        //BJG 10/12/14: Added option to NOT extrapolate.
        {
            Triangulation TriangClass = new Triangulation();
            Edge[] EdgeList=new Edge[0];
            Triangle[] TriList = new Triangle[0];
            TriangClass.Delaunay2D(PtList, ref TriList, ref EdgeList, ARMax);
            int NTris = TriList.GetLength(0);
            //Calculate coefficients of planes fitted through each triangle:
            double[,] TriCoeffs = TriangClass.TriPlaneCoeffs(TriList, EdgeList, PtList);
            //Get convex hull:
            int[] iCHull = TriangClass.ConvHull(EdgeList);
            int NCHull = iCHull.GetLength(0);
            //Get convex hull normals:
            double[,] dHullNormals = TriangClass.ConvHullNormals(PtList, EdgeList, iCHull, TriList);
            //Get average normals at each vertex around the convex hull:
            Point2D[] AvHullNormals = TriangClass.AvConvHullNormals(EdgeList, iCHull, dHullNormals, TriCoeffs);
            //Dummy variable required for interpolation routine - this is not used here, but is set to true if
            //the value is extrapolated rather than interpolated.
            bool flOutside = false;
            //Loop through input, interpolating values:
            Point2D TestPoint = new Point2D();
            //Create array to hold output:
            int NOutPts = dblQueryPts.GetLength(0);
            double[] dblOutput = new double[NOutPts];
            for (int i = 0; i < NOutPts; i++)
            {
                TestPoint.X = dblQueryPts[i, 0];
                TestPoint.Y = dblQueryPts[i, 1];
                TriangClass.InterpVal(ref TestPoint, PtList, EdgeList, TriList,NTris, TriCoeffs, iCHull, NCHull,
                                                    AvHullNormals, ref flOutside, flExtrapolate);
                dblOutput[i] = TestPoint.Value;
                flOutsideHull[i] = flOutside;
            }
            return dblOutput;
        }

        public double[,] CalcPlaneCoeffs(double[,] dblPtList, float ARMax)
        /* Given a list of points X, Y, and Z, the routine triangulates the points, and returns a list of X and Y coordinates of triangle
         * centres, and the plane fit coefficients for each triangle.
         * Coefficients of plane fit are XCoeff, YCoeff, Offset.*/
        {
            Triangulation TriangClass = new Triangulation();
            Edge[] EdgeList = new Edge[0];
            Triangle[] TriList = new Triangle[0];
            int NPts = dblPtList.GetLength(0);
            Point2D[] PtList = new Point2D[NPts];
            for (int i=0;i<NPts;i++)
            {
                PtList[i] = new Point2D();
                PtList[i].X=dblPtList[i,0];
                PtList[i].Y=dblPtList[i,1];
                PtList[i].Value=dblPtList[i,2];
            }
            TriangClass.Delaunay2D(PtList, ref TriList, ref EdgeList, ARMax);
            //Calculate coefficients of planes fitted through each triangle:
            double[,] TriCoeffs = TriangClass.TriPlaneCoeffs(TriList, EdgeList, PtList);
            //Store data:
            int NTris = TriList.GetLength(0);
            double[,] dblResult = new double[NTris, 5];
            for (int i = 0; i < NTris; i++)
            {
                dblResult[i, 0] = (PtList[EdgeList[TriList[i].FirstEdge].P1].X + PtList[EdgeList[TriList[i].FirstEdge].P2].X + PtList[TriList[i].ThirdPt].X) / 3;
                dblResult[i, 1] = (PtList[EdgeList[TriList[i].FirstEdge].P1].Y + PtList[EdgeList[TriList[i].FirstEdge].P2].Y + PtList[TriList[i].ThirdPt].Y) / 3;
                dblResult[i, 2] = TriCoeffs[i, 0];
                dblResult[i, 3] = TriCoeffs[i, 1];
                dblResult[i, 4] = TriCoeffs[i, 2];
            }
            return dblResult;
        }

        public double[,] TriangulationQuality(double[,] dblPtList, float ARMax)
        /* Given a list of points X and Y, the routine triangulates the points, and returns a list of X and Y coordinates of triangle
         * centres, and the area and aspect ratio of each triangle.
         * Result is in the form X,Y,Area, Aspect ratio.*/
        {
            Triangulation TriangClass = new Triangulation();
            Edge[] EdgeList = new Edge[0];
            Triangle[] TriList = new Triangle[0];
            int NPts = dblPtList.GetLength(0);
            Point2D[] PtList = new Point2D[NPts];
            for (int i = 0; i < NPts; i++)
            {
                PtList[i] = new Point2D();
                PtList[i].X = dblPtList[i, 0];
                PtList[i].Y = dblPtList[i, 1];
            }
            //Calculate triangulation:
            TriangClass.Delaunay2D(PtList, ref TriList, ref EdgeList, ARMax);
            //Store data:
            int NTris = TriList.GetLength(0);
            double[,] dblResult = new double[NTris, 4];
            for (int i = 0; i < NTris; i++)
            {
                dblResult[i, 0] = (PtList[EdgeList[TriList[i].FirstEdge].P1].X + PtList[EdgeList[TriList[i].FirstEdge].P2].X + PtList[TriList[i].ThirdPt].X) / 3;
                dblResult[i, 1] = (PtList[EdgeList[TriList[i].FirstEdge].P1].Y + PtList[EdgeList[TriList[i].FirstEdge].P2].Y + PtList[TriList[i].ThirdPt].Y) / 3;
                dblResult[i, 2] = TriList[i].TriArea(PtList, EdgeList);
                dblResult[i, 3] = TriList[i].AR(PtList, EdgeList);
            }
            return dblResult;
        }
    }
}
