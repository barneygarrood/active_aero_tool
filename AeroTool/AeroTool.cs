﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace AeroTool
{  
    /// <summary>
    /// Aerotool interpolation class
    /// Author: Barnaby Garrood
    /// Date: 10/12/2014
    /// </summary>
    /// 

    public interface IAeroModelInterface
    {
        double[,] InterpolateMap(ref double[,] dblMapData, ref double[] dblTolerance, ref double[,] dblSetPts);
        double[,] InterpolateMapV2(ref double[,] dblMapData, ref double[] dblTolerance, ref bool[] flSymmetric, ref double[,] dblSetPts, 
                                        ref bool flExtrapRideMap, ref bool flExtrapRideSens, ref bool flExtrapSens);
    }
    //Make class COM visible:
    [ComVisible(true)]
    //Force to use an explicitly defined interface:
    [ClassInterface(ClassInterfaceType.None)]
    public class AeroModelInterface : IAeroModelInterface
    {
        private Logger LGR = new Logger();

        //Default constructor:
        public AeroModelInterface() { }

        //First COM visible function.
        [ComVisible(true)]
        public double[,] InterpolateMap(ref double[,] dblMapData, ref double[] dblTolerance, ref double[,] dblSetPts)
        //Old style to retain backwards compatability with old versions of aerotool - assumes extrapolation everywhere.
        //Author: Barney Garrood
        //Date: 10/12/14
        {
            bool flExtrapRideMap = true;
            bool flExtrapRideSens = true;
            bool flExtrapSens = true;

            bool[] flSymmetric = new bool[dblTolerance.GetLength(0)];
            double[,] dblOut=InterpolateMapV2(ref dblMapData,ref dblTolerance,ref flSymmetric,ref dblSetPts,ref flExtrapRideMap,ref flExtrapRideSens, ref flExtrapSens);
            return dblOut;
        }

        //Second COM visible funciton - allows turning off extrapolation.
        [ComVisible(true)]
        public double[,] InterpolateMapV2(ref double[,] dblMapData, ref double[] dblTolerance, ref bool[] flSymmetric, ref double[,] dblSetPts, 
                                ref bool flExtrapRideMap, ref bool flExtrapRideSens, ref bool flExtrapSens)
        //Interpolation routine.
        //Inputs are aeromap setpoints (FRH,RRH,Roll,Steer,Yaw, although last 3 can be in any order or may be missing)
        //aeromap data (SCzF,SCzR,SCx etc - number of parameters is not important), and parameter list (includes definition if each paramter is symmetric or not)
        //and list of setpoints at which to interpolate - parameters must be same as in aero map.
        //Output is set of parameters for each input setpoint.
        //Author: Barney Garrood
        //Date: 13/01/14
        //Revised: 10/12/14
        //Removed parameters as input for compatability with VBA.
        //Generalised number of aero parameters, and separate boolean array to say whether symmetric or not.
        //Assume number of Coefficients is number of columns in dblMapData minus number of solumns in dblSetPts
        {
            LGR.CreateLog();
            LGR.Log("Start.");
            int iNIn = dblMapData.GetLength(0);
            int iNOut = dblSetPts.GetLength(0);
            int iNParams = dblSetPts.GetLength(1)-2;
            int iNCoeffs = dblMapData.GetLength(1) - dblSetPts.GetLength(1); 

            //1. Get list of discrete ride points - FRH/RRH pairs at each roll/steer/yaw etc combination.
            //2. Construct sensitvity maps, i.e. over FRH/RRH maps of sensitivity of each aeroforce component to roll/steer/yaw.
            //3. Interpolate/extrapolate missing rideheight points for sensitivity maps using mapomatic.
            //4. Use sensitivities to calculte aero forces at each given rideheight 

            //Get discrete parameter values:
            double[,] dblUniqueRides = GetUniqueRHs(dblMapData, dblTolerance);
            List<ParamSensitivity> ParamSens = new List<ParamSensitivity>();
            for (int i = 0; i < iNParams; i++)
            {
                ParamSens.Add(new ParamSensitivity()); 
                double[] dblVals = GetUniqueParam(dblMapData, dblTolerance, i, flSymmetric[i]);
                ParamSens[i].dblVal = dblVals;
                ParamSens[i].NVals = dblVals.GetLength(0);
            }


            //Symmeterise map - just means taking absolute values of symmetric parameters.
            LGR.Log("Symmeterise.");
            SymmeteriseMap(dblMapData, flSymmetric,iNParams);
            //Calculate sensitivities:
            int[] iBaseMapIndex = new int[1];
            LGR.Log("GetSensitivities.");
            GetSensitivities(dblMapData, dblUniqueRides, dblTolerance, ParamSens, iNCoeffs,ref iBaseMapIndex);
            //Interpolate/Extrapolate missing points in sensitivity points.
            LGR.Log("ExtrapolateSensitivities.");
            ExtrapolateSensitivities(dblUniqueRides, dblTolerance, ParamSens, iNCoeffs, flExtrapRideSens);

            LGR.Log("Get base map.");
            //Get Base Map
            List<int> iBaseMapPts = new List<int>();
            for (int i = 0; i < dblUniqueRides.GetLength(0); i++)
            {
                for (int j = 0; j < iNIn; j++)
                {
                    bool fl = true;
                    for (int k = 0; k < 2; k++)         //2 means FRH, RRH
                    {
                        if (Math.Abs(dblMapData[j, k] - dblUniqueRides[i, k]) > dblTolerance[k])
                        {
                            fl = false;
                        }
                    }
                    for (int k = 0; k < iNParams; k++)
                    {
                        if (Math.Abs(dblMapData[j, k + 2]-ParamSens[k].dblVal[iBaseMapIndex[k]]) > dblTolerance[k])       //Just looking for zeros.
                        {
                            fl = false;
                        }
                    }
                    if (fl)
                    {
                        iBaseMapPts.Add(j);
                        break;
                    }
                }
            }
            //Store map - only need rideheight and coefficient:
            double[,] dblBaseMap = new double[iBaseMapPts.Count,2+iNCoeffs];
            for (int i=0;i<iBaseMapPts.Count;i++)
            {
                for (int j=0;j<2;j++)
                {
                    dblBaseMap[i,j]=dblMapData[iBaseMapPts[i],j];
                }
                for (int j=0;j<iNCoeffs;j++)
                {
                    dblBaseMap[i,j+2]=dblMapData[iBaseMapPts[i],j+2+iNParams];
                }
            }
            LGR.Log("Base map:");
            for (int i = 0; i < iBaseMapPts.Count; i++)
            {
                string strBase = "";
                for (int j = 0; j < 2 + iNCoeffs; j++)
                {
                    strBase = strBase + dblBaseMap[i, j].ToString() + ", ";
                }
                LGR.Log(strBase);
            }


            //Note this part needs to run for each set of setpoints for which we want to interpolate the data.
            //Symmeterise input map:
            LGR.Log("Symmeterise input map.");
            SymmeteriseMap(dblSetPts, flSymmetric, iNParams);     //Just converts parameter values to aboslute values if paramter is symmetric.
            LGR.Log("InterpolateInner.");
            double[,] dblOut = InterpolateInner(dblSetPts, dblUniqueRides, dblBaseMap, ParamSens, iNCoeffs, iBaseMapIndex, flExtrapRideMap, flExtrapRideSens, flExtrapSens);
            LGR.Log("Done - returning.");
            LGR.StopTimer();
            return dblOut;
        }

        public void SymmeteriseMap(double[,] dblIn, bool[] flSymmetric, int iNParams)
        //Converts map paramters to absolute values if paramter is symmetric.
        //Note parameters do not include FRH, RRH.
        //Author: Barney Garrood
        //Date: 01/13/14
        {
            for (int i = 0; i < iNParams; i++)
            {
                if (flSymmetric[i])
                {
                    for (int j = 0; j < dblIn.GetLength(0); j++)
                    {
                        dblIn[j, i + 2] = Math.Abs(dblIn[j, i + 2]);
                    }
                }
            }
        }

        public double[,] InterpolateInner(double[,] dblSetPts, double[,] dblUniqueRides, double[,] dblBaseMap, List<ParamSensitivity> ParamSens, int iNCoeffs,
                                                int[] iBaseMapIndex, bool flExtrapRideMap, bool flExtrapRideSens, bool flExtrapSens)
        //Given the already calculated sensitivity maps, we interpolate each given setpoint.
        //I havent figured out a decent way of interpolating asymmetric yet.
        //Author: Barney Garrood
        //Date: 01/13/14
        //Update: 10/12/14
        //Change to allow symmetric - just assume continuous..
        {
            Interpolate InterpClass = new Interpolate();
            int iNSetPts = dblSetPts.GetLength(0);
            int iNMap = dblUniqueRides.GetLength(0);
            int iNBaseMap = dblBaseMap.GetLength(0);
            int iNParams = ParamSens.Count;
            double[,] dblOut = new double[iNSetPts, iNCoeffs];
            float ARMax = 10f;
            //Setup input points matrix, which is FRH, RRH, sensitivity:

            double[,] dblInPts = new double[iNBaseMap, 3];
            for (int i = 0; i < iNBaseMap; i++)
            {
                dblInPts[i, 0] = dblBaseMap[i, 0];
                dblInPts[i, 1] = dblBaseMap[i, 1];
            }
            //Setup output points matrix:
            double[,] dblOutPts = new double[iNSetPts, 2];
            //Put rideheights in first:
            for (int i = 0; i < iNSetPts; i++)
            {
                dblOutPts[i, 0] = dblSetPts[i, 0];
                dblOutPts[i, 1] = dblSetPts[i, 1];
            }
            //Loop through coefficients, interpolating each in turn.
            for (int i = 0; i < iNCoeffs; i++)
            {
                //Start by interpolating rideheight:
                //Add coefficient into input array:
                for (int j = 0; j < iNBaseMap; j++)
                {
                    dblInPts[j, 2] = dblBaseMap[j, i+2];  
                }
                //Interpolate rideheight value (on base map)
                double[] dblTemp = InterpClass.InterpValsExtrapOption(dblInPts, dblOutPts, ARMax, flExtrapRideMap);
                //Put result into output matrix:
                for (int j = 0; j < iNSetPts; j++)
                {
                    dblOut[j, i] = dblTemp[j];
                }

                //Go through each paramter, doing piecewise linear interpolation on each:
                for (int j = 0; j < iNParams; j++)
                {
                    //Note that base map could be at any parameter config.
                    //So we need to add deltas in correct direction:
                    //Firstly calculate all sensitivites at each rideheight:
                    List<double[]> lAllSens = new List<double[]>();
                    for (int k = 0; k < ParamSens[j].NVals - 1; k++)
                    {
                        //Add sensitivities into input array:
                        for (int l = 0; l < iNMap; l++)
                        {
                            dblInPts[l, 2] = ParamSens[j].dblSens[l, k, i];
                        }
                        //Interpolate:
                        dblTemp = InterpClass.InterpValsExtrapOption(dblInPts, dblSetPts, ARMax, flExtrapRideSens);
                        lAllSens.Add(dblTemp);
                    }
                    //Now go through setpoints, and apply deltas:
                    for (int k = 0; k < iNSetPts; k++)
                    {
                        if (dblSetPts[k, j + 2] > ParamSens[j].dblVal[iBaseMapIndex[j]])
                        {
                            //Do interpolation going up, stepping through:
                            for (int l = iBaseMapIndex[j]; l < ParamSens[j].NVals - 1; l++)
                            {
                                if (dblSetPts[k, j + 2] > ParamSens[j].dblVal[l + 1])
                                {
                                    //Step over division
                                    dblOut[k, i] += lAllSens[l][k] * (ParamSens[j].dblVal[l + 1] - ParamSens[j].dblVal[l]);
                                }
                                else
                                {
                                    //Interpolate within division
                                    dblOut[k, i] += lAllSens[l][k] * (dblSetPts[k, j + 2] - ParamSens[j].dblVal[l]);
                                    break;
                                }
                            }
                            //Extrapolate out if necessary:
                            if (dblSetPts[k, j + 2] > ParamSens[j].dblVal[ParamSens[j].NVals - 1])
                            {
                                if (flExtrapSens)
                                {
                                    dblOut[k, i] += (dblSetPts[k, j + 2] - ParamSens[j].dblVal[ParamSens[j].NVals - 1]) * lAllSens[ParamSens[j].NVals - 2][k];
                                }
                            }
                        }
                        else
                        {
                            //Do interpolation going up, stepping through:
                            for (int l = iBaseMapIndex[j]; l > 0; l--)
                            {
                                if (dblSetPts[k, j + 2] < ParamSens[j].dblVal[l-1])
                                {
                                    //Step over division
                                    dblOut[k, i] += lAllSens[l-1][k] * (ParamSens[j].dblVal[l - 1] - ParamSens[j].dblVal[l]);
                                }
                                else
                                {
                                    //Interpolate within division
                                    dblOut[k, i] += lAllSens[l-1][k] * (dblSetPts[k, j + 2] - ParamSens[j].dblVal[l]);
                                    break;
                                }
                            }
                            //Extrapolate out if necessary:
                            if (dblSetPts[k, j + 2] < ParamSens[j].dblVal[0])
                            {
                                if (flExtrapSens)
                                {
                                    dblOut[k, i] += (dblSetPts[k, j + 2] - ParamSens[j].dblVal[0]) * lAllSens[0][k];
                                }
                            }

                        }
                    }
                }
            }
            return dblOut;
        }


        public void ExtrapolateSensitivities(double[,] dblUniqueRides, double[] dblTolerance, List<ParamSensitivity> ParamSens, int iNCoeffs, bool flExtrapRideSens)
        //Interpolates/extrapolates sensitivities to reconstruct complete ride map.
        //Author: Barney Garrood
        //Date: 01/13/14
        //Update: 10/12/14
        //Made generalised number of coeffs.
        {

            LGR.Log("In ExtrapolateSensitivities");
            Interpolate InterpClass = new Interpolate();
            int iNParams = ParamSens.Count;
            int iNRides = dblUniqueRides.GetLength(0);
            float ARMax = 100.0f;
            for (int i = 0; i < iNParams; i++)
            {
                int iNSens = ParamSens[i].NVals - 1;
                //Count number of rides for which sensitivity values exist:
                for (int j = 0; j < iNSens; j++)
                {
                    int iNSensRides = 0;
                    for (int k = 0; k < iNRides; k++)
                    {
                        if (ParamSens[i].flSens[k, j])
                        {
                            iNSensRides++;
                        }
                    }
                    //We have 3 options.  If there is one sensitivity, we apply across map.
                    //If there are two, we take average and apply across map.
                    //If there are 3 we interpolate/extrapolate the surface.
                    //If there are none, then we leave sensitivities as zero (already like that).
                    if ((iNSensRides >= 1) && (iNSensRides <= 2))
                    {
                        double[] dblSens = new double[iNCoeffs];
                        for (int k = 0; k < iNCoeffs; k++)
                        {
                            dblSens[j] = 0;
                        }
                        for (int k = 0; k < iNRides; k++)
                        {
                            if (ParamSens[i].flSens[k, j])
                            {
                                for (int l = 0; l < iNCoeffs; l++)
                                {
                                    dblSens[l] += ParamSens[i].dblSens[k, j, l] / iNSensRides;
                                }
                            }
                        }
                        //Put back average value into all rides:
                        for (int k = 0; k < iNRides; k++)
                        {
                            for (int l = 0; l < iNCoeffs; l++)
                            {
                                ParamSens[i].dblSens[k, j, l] = dblSens[l];
                            }
                        }
                    }
                    else if (iNSensRides > 2)
                    {
                        //Build up arrays to pass to interpolator:
                        double[,] dblDataPts = new double[iNSensRides, 3];
                        for (int l = 0; l < iNCoeffs; l++)
                        {
                            int iCount = 0;
                            for (int k = 0; k < iNRides; k++)
                            {                                           
                                if (ParamSens[i].flSens[k, j])
                                {
                                    dblDataPts[iCount, 0] = dblUniqueRides[k, 0];
                                    dblDataPts[iCount, 1] = dblUniqueRides[k, 1];
                                    dblDataPts[iCount, 2] = ParamSens[i].dblSens[k, j, l];
                                    iCount++;
                                }

                            }
                            //!!!!!!!!!!!!!!!!!!!!!!!!!!!! NEED TO MODIFY HERE TO ALLOW PREVENTION OF EXTRAPOLATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            LGR.Log(string.Format("             DoneA"));
                            for (int k = 0; k < dblDataPts.GetLength(0); k++)
                            {
                                string str = string.Format("dblDataPts[{0},l] = ",k);
                                for (int m = 0; m < dblDataPts.GetLength(1); m++)
                                {
                                    str = str + string.Format("{0:0.00},", dblDataPts[k, m]);
                                }
                                LGR.Log(str);
                            }
                            for (int k = 0; k < dblUniqueRides.GetLength(0); k++)
                            {
                                string str = string.Format("dblUniqueRides[{0},l] = ", k);
                                for (int m = 0; m < dblUniqueRides.GetLength(1); m++)
                                {
                                    str = str + string.Format("{0:0.00},", dblUniqueRides[k, m]);
                                }
                                LGR.Log(str);
                            }
                            LGR.Log(string.Format("ARMax = {0}", ARMax));
                            LGR.Log(string.Format("flExtrapRideSens = {0}", flExtrapRideSens));

                            double[] dblOut = InterpClass.InterpValsExtrapOption(dblDataPts, dblUniqueRides, ARMax, flExtrapRideSens);
                            LGR.Log(string.Format("             DoneB"));
                            for (int k = 0; k < iNRides; k++)
                            {
                                ParamSens[i].dblSens[k, j, l] = dblOut[k];
                            }
                        }
                    }
                }
            }
        }

        public void GetSensitivities(double[,] dblMapData, double[,] dblUniqueRides, double[] dblTolerance, List<ParamSensitivity> ParamSens, int iNCoeffs, ref int[] iBaseMapIndex)
        //For each rideheight, and each parameter, calculates sensitivities.]
        //Note that we allow sensitivities to vary with rideheight, but parameters (yaw/roll/steer etc) are considered independent of one another.
        //Author: Barney Garrood
        //Date: 01/13/14
        //Update: 10/12/14
        //Generalised to any number of parameters
        //Allow non-symmetric parameters.
        {
            int iNParams = ParamSens.Count;
            int iNRides = dblUniqueRides.GetLength(0);
            int iNSetpts = dblMapData.GetLength(0);
            bool[] flSetpts = new bool[iNSetpts];   //Flag to say if a setpt is already used in defining a parameter sensitivity.
            //Index list of setpoints - makes it more efficient:
            int[,] iMapIndex = new int[iNSetpts, iNParams + 1];
            for (int i = 0; i < iNSetpts; i++)
            {
                //Do rideheight first:
                iMapIndex[i, 0] = -1;          
                for (int j = 0; j < iNRides; j++)
                {
                    if ((Math.Abs(dblMapData[i, 0] - dblUniqueRides[j, 0]) < dblTolerance[0]) && (Math.Abs(dblMapData[i, 1] - dblUniqueRides[j, 1]) < dblTolerance[1]))
                    {
                        iMapIndex[i, 0] = j;
                        break;
                    }
                }
                //Then other parameters:
                for (int j = 0; j < iNParams; j++)
                {
                    iMapIndex[i, j + 1] = 0;
                    for (int k = 0; k < ParamSens[j].NVals; k++)
                    {
                        if (Math.Abs(dblMapData[i, j + 2] - ParamSens[j].dblVal[k]) < dblTolerance[j + 2])
                        {
                            iMapIndex[i, j + 1] = k;
                            break;
                        }
                    }
                }
            }

           //Need to find config at which greatest number of rides exists:
            bool[] flRideExists = new bool[iNRides];
            iBaseMapIndex = new int[iNParams];
            int[] iTemp= new int[iNParams];
            int iMax = 0;
            for (int i = 0; i < iNSetpts - 1; i++)
            {
                //Get current parameter indices:
                for (int j = 0; j < iNParams; j++)
                {
                    for (int k = 0; k < ParamSens[j].NVals; k++)
                    {
                        if (Math.Abs(dblMapData[i, j + 2] - ParamSens[j].dblVal[k]) < dblTolerance[j + 2])
                        {
                            iTemp[j] = k;
                            break;
                        }
                    }
                }
                //Tick off number of setpts:
                int iNSetPtsFound = 0;
                for (int j = 0; j < iNRides; j++)
                {
                    for (int k = i; k < iNSetpts; k++)
                    {
                        bool fl = true;
                        for (int l = 0; l < 2; l++)
                        {
                            if ( Math.Abs(dblMapData[k,l]-dblUniqueRides[j,l]) > dblTolerance[l])
                            {
                                fl=false;
                                break;
                            }
                        }
                        if (!fl) { continue; }
                        for (int l = 0; l < iNParams; l++)
                        {
                            if (Math.Abs(dblMapData[k, l+2] - ParamSens[l].dblVal[iTemp[l]]) > dblTolerance[l+2])
                            {
                                fl = false;
                                break;
                            }
                        }
                        if (fl)
                        {
                            iNSetPtsFound++;
                            break;
                        }
                    }
                }
                if (iNSetPtsFound > iMax) 
                    { 
                    iTemp.CopyTo(iBaseMapIndex,0) ;
                    if (iNSetPtsFound == iNRides) { break; }        //Full house!  No need to search further.
                }
            }


            for (int i = 0; i < iNParams; i++)
            {
                int iNSens = ParamSens[i].NVals - 1;        //i.e. number of sensitivities is number of parameters - 1
                ParamSens[i].dblSens = new double[iNRides, iNSens, iNCoeffs];
                ParamSens[i].flSens = new bool[iNRides, iNSens];
                //Initialise values:
                int[,] iNPairs = new int[iNRides, iNSens];  //Count of pairs defining each sensitivity.
                for (int j = 0; j < iNRides; j++)
                {
                    for (int k = 0; k < ParamSens[i].NVals - 1; k++)
                    {
                        for (int l = 0; l < iNCoeffs; l++)
                        {
                            ParamSens[i].dblSens[j, k, l] = 0.0;
                        }
                        ParamSens[i].flSens[j, k] = false;
                    }
                    for (int k = 0; k < iNSens; k++)
                    {
                        iNPairs[j, k] = 0;
                    }
                }
                for (int j = 0; j < iNSetpts - 1; j++)
                {
                    //Loop through remaining points and see what can be used:
                    for (int k = j + 1; k < iNSetpts; k++)
                    {
                        bool fl = true;
                        //Is rideheight the same?
                        for (int l = 0; l < iNParams + 1; l++)
                        {
                            if (l == i + 1)     //i.e. are we looking at the same parameter?  If so we want values to be one delta apart.
                            {
                                if (Math.Abs(iMapIndex[j, l] - iMapIndex[k, l]) != 1)       //i.e. we only want one delta part.  
                                {
                                    fl = false;
                                    break;
                                }
                            }
                            else
                            {
                                if (iMapIndex[j, l] != iMapIndex[k, l])     //i.e. if we are comparing different parameters from sensitivity parameter
                                {                                           //they must be the same.
                                    fl = false;
                                    break;
                                }
                            }
                        }
                        if (fl)         //Have we found a matching pair, where all params and RH are the same, apart from sensitivity parameter which is one index apart:
                        {
                            int iPos = 0;
                            //We have a pair of setpoints we can use.  Find which position it sits in:
                            if (iMapIndex[j, i + 1] > iMapIndex[k, i + 1])
                            {
                                iPos = iMapIndex[k, i + 1];
                            }
                            else
                            {
                                iPos = iMapIndex[j, i + 1];
                            }
                            ParamSens[i].flSens[iMapIndex[j, 0], iPos] = true;
                            iNPairs[iMapIndex[j, 0], iPos]++;               //One more sensitivity found for this rideheight.  Could have more than one pair as at different values of other paramters..?
                            for (int l = 0; l < iNCoeffs; l++)
                            {
                                ParamSens[i].dblSens[iMapIndex[j, 0], iPos, l] += (dblMapData[j, 2 + iNParams + l] - dblMapData[k, 2 + iNParams + l]) / (dblMapData[j, 2 + i] - dblMapData[k, 2 + i]);
                            }
                        }
                    }
                }
                //Normalise where we have multiple pairs:
                for (int j = 0; j < iNRides; j++)
                {
                    for (int k = 0; k < ParamSens[i].NVals - 1; k++)
                    {
                        if (iNPairs[j, k] > 1)
                        {
                            for (int l = 0; l < iNCoeffs; l++)
                            {
                                ParamSens[i].dblSens[j, k, l] /= iNPairs[j, k];
                            }
                        }
                    }
                }
            }
        }


        public double[,] GetUniqueRHs(double[,] dblMapData, double[] dblTolerance)
        //Returns list of unique FRH/RRH pairs
        //Author: Barney Garrood
        //Date: 01/13/14
        {
            double[,] dblTemp;
            double[,] dblOut;
            int NRides;
            int NUnique = 0;
            bool fl;
            NRides = dblMapData.GetLength(0) + 1;
            dblTemp = new double[NRides, 2];

            for (int i = 0; i < NRides - 1; i++)
            {
                fl = true;
                for (int j = 0; j < NUnique; j++)
                {
                    if ((Math.Abs(dblMapData[i, 0] - dblTemp[j, 0]) < dblTolerance[0]) && (Math.Abs(dblMapData[i, 1] - dblTemp[j, 1]) < dblTolerance[1]))
                    {
                        fl = false;
                        break;
                    }
                }
                if (fl)
                {
                    dblTemp[NUnique, 0] = dblMapData[i, 0];
                    dblTemp[NUnique, 1] = dblMapData[i, 1];
                    NUnique++;
                }
            }

            //Resize output array, and put in data:
            dblOut = new double[NUnique, 2];
            for (int i = 0; i < NUnique; i++)
            {
                dblOut[i, 0] = dblTemp[i, 0];
                dblOut[i, 1] = dblTemp[i, 1];
            }
            return dblOut;
        }

        public double[] GetUniqueParam(double[,] dblMapData, double[] dblTolerance, int iParam, bool sym)
        //Returns list of unique FRH/RRH pairs if iParamth parameter.
        //Note that first parameter is iParam=0 (i.e. if we have FRH,RRH,XW,Roll,Steer, then iParam = 0 represents XW.
        //Author: Barney Garrood
        //Date: 01/13/14
        {
            double[] dblTemp;
            double[] dblOut;
            int NRides;
            int NUnique;
            bool fl;
            double dblVal;
            NUnique = 0;
            NRides = dblMapData.GetLength(0);
            dblTemp = new double[NRides];
            for (int i = 0; i < NRides; i++)
            {
                fl = true;
                if (sym)
                {
                    dblVal = Math.Abs(dblMapData[i, 2 + iParam]);
                }
                else
                {
                    dblVal = dblMapData[i, 2 + iParam];
                }
                for (int j = 0; j < NUnique; j++)
                {
                    if (Math.Abs(dblVal - dblTemp[j]) < dblTolerance[iParam + 2])
                    {
                        fl = false;
                        break;
                    }
                }
                if (fl)
                {
                    dblTemp[NUnique] = dblVal;
                    NUnique++;
                }
            }
            //Resize output array, and put in data:
            dblOut = new double[NUnique];
            for (int i = 0; i < NUnique; i++)
            {
                dblOut[i] = dblTemp[i];
            }
            //Sorst list:
            dblOut = SortList(dblOut);
            return dblOut;
        }

        public double[] SortList(double[] dblIn)
        //Sorts a list of numbers, small to large by bubble sort.
        //Author: Barney Garrood
        //Date: 01/13/14
        {
            double[] dblOut;
            int iNPts;
            double dblTemp;
            iNPts = dblIn.GetLength(0);
            dblOut = new double[iNPts];
            dblOut[0] = dblIn[0];
            for (int i = 1; i < iNPts; i++)
            {
                dblOut[i] = dblIn[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (dblOut[j + 1] < dblOut[j])
                    {
                        dblTemp = dblOut[j + 1];
                        dblOut[j + 1] = dblOut[j];
                        dblOut[j] = dblTemp;
                    }
                }
            }
            return dblOut;
        }

        //Data structure classes
        //Defines the way that the calculated sensitivities are stored within the model.
        //Author: B. Garrood
        //Date: 01/13/2014

        public class ParamSensitivity
        //Definition of parameter sensitivity definition.
        {
            public int NVals;
            public double[] dblVal;    //Ordered list of parameter values.
            public double[, ,] dblSens; //1st index = RH, 2nd = number of sensitivity (i.e 0-4degrees, 4-6 degrees etc), 3rd index = aero value (SCx,SCzF etc)
            public bool[,] flSens;      //Boolean array which is true for parameter and RH if a sensitivity exists.
        }

    }
}
