using System;
using System.Collections.Generic;
using System.Text;

namespace AeroTool
{
    public class Edge
    {
        public int P1;
        public int P2;
        public int Tri;
        public int OppEdge;
        public int NextEdge;
        public bool Visible;
        public bool Mark;

        public Edge()
        //Constructor:
        {
            P1 = -1;
            P2 = -1;
            Tri = -1;
            OppEdge = -1;
            NextEdge = -1;
            Visible = false;
            Mark = false;
        }

        public double Length(Point2D[] PtList)
        //Returns the length of the edge.
        {
            double dblLen = Math.Sqrt(Math.Pow(PtList[P2].X - PtList[P1].X, 2) + Math.Pow(PtList[P2].Y - PtList[P1].Y,2));
            return dblLen;
        }

        public bool TriAreaPositive(Point2D[] PtList, int QueryPt)
        //Returns true if area of triangle made with edge and a query point is greater than zero
        //(in fact greater than a small number, to remove rounding errors)
        {
            double small = 1.0e-10;
            double[,] dblTest = new double[3, 3];
            NxNMatrix TestMatrix = new NxNMatrix(3);
            dblTest[0, 0] = PtList[P1].X;
            dblTest[0, 1] = PtList[P1].Y;
            dblTest[1, 0] = PtList[P2].X;
            dblTest[1, 1] = PtList[P2].Y;
            dblTest[2, 0] = PtList[QueryPt].X;
            dblTest[2, 1] = PtList[QueryPt].Y;
            for (int i = 0; i < 3; i++)
            {
                dblTest[i, 2] = 1;
            }
            TestMatrix.Elements = dblTest;
            if (TestMatrix.Determinant() > small)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Edge Copy()
        //Creates a copy of the edge - this is to avoid referencing problems.
        {
            Edge EdgeCopy = new Edge();
            EdgeCopy.P1=P1;
            EdgeCopy.P2=P2;
            EdgeCopy.Tri=Tri;
            EdgeCopy.Visible=Visible;
            EdgeCopy.Mark=Mark;
            EdgeCopy.NextEdge=NextEdge;
            EdgeCopy.OppEdge=OppEdge;
            return EdgeCopy;
        }

        public double[] UnitNormal(Point2D[] PtList)
        //Returns the unit normal vector to the edge.
        {
            double[] NormVec = new double[2];
            double[] DirVec=new double[2];
            DirVec[0]=PtList[P2].X-PtList[P1].X;
            DirVec[1]=PtList[P2].Y-PtList[P1].Y;
            double Mag = Math.Sqrt(DirVec[0] * DirVec[0] + DirVec[1] * DirVec[1]);
            NormVec[0] = DirVec[1] / Mag;
            NormVec[1] = -DirVec[0] / Mag;
            return NormVec;
        }

    }
}
