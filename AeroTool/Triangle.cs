using System;
using System.Collections.Generic;
using System.Text;

namespace AeroTool
{
    public class Triangle
    {
        public int ThirdPt;
        public int FirstEdge;
        public bool Mark;

        public Triangle()
        //Constructor
        {
            ThirdPt=-1;
            FirstEdge=-1;
            Mark=false;
        }

        public double AR(Point2D[] PtList, Edge[] EdgeList)
        //Calculates aspect ratio of trianle:
        {
            //Get longest edge length:
            double dblLongest = 0;
            int iEdge = FirstEdge;
            int iLongestEdge;
            for (int i = 0; i < 3; i++)
            {
                double dblLen = EdgeList[iEdge].Length(PtList);
                if (dblLen > dblLongest)
                {
                    iLongestEdge = iEdge;
                    dblLongest = dblLen;
                }
                iEdge = EdgeList[iEdge].NextEdge;
            }
            //Calculate aspect ratio:
            double dblAR = Math.Pow(dblLongest, 2) / TriArea(PtList, EdgeList) / 2d;
            return dblAR;
        }
        
        public double TriArea(Point2D[] PtList, Edge[] EdgeList)
        //Calculates area of triangle:
        {
            NxNMatrix TestMat = new NxNMatrix(3);
            double[,] dblMat = new double[3, 3];
            dblMat[0, 0] = PtList[EdgeList[FirstEdge].P1].X;
            dblMat[1, 0] = PtList[EdgeList[FirstEdge].P2].X;
            dblMat[2, 0] = PtList[ThirdPt].X;
            dblMat[0, 1] = PtList[EdgeList[FirstEdge].P1].Y;
            dblMat[1, 1] = PtList[EdgeList[FirstEdge].P2].Y;
            dblMat[2, 1] = PtList[ThirdPt].Y;
            for (int i = 0; i < 3; i++)
            {
                dblMat[i, 2] = 1;
            }
            TestMat.Elements = (double[,])dblMat.Clone();
            double dblArea = 1.0f / 2.0f * TestMat.Determinant();
            return dblArea;
        }

        public bool PtInCircumcircle(Point2D[] PtList, Edge[] Edgelist, int TestPoint)
        //Returns true if the test point is inside the triangle's circumcircle.
        {
            NxNMatrix TestMat = new NxNMatrix(4);
            double[,] dblMat = new double[4, 4];
            double dblSmall = 1e-6;
            dblMat[0, 0] = PtList[Edgelist[FirstEdge].P1].X;
            dblMat[0, 1] = PtList[Edgelist[FirstEdge].P1].Y;
            dblMat[1, 0] = PtList[Edgelist[FirstEdge].P2].X;
            dblMat[1, 1] = PtList[Edgelist[FirstEdge].P2].Y;
            dblMat[2, 0] = PtList[ThirdPt].X;
            dblMat[2, 1] = PtList[ThirdPt].Y;
            dblMat[3, 0] = PtList[TestPoint].X;
            dblMat[3, 1] = PtList[TestPoint].Y;
            for (int i = 0; i < 4; i++)
            {
                dblMat[i, 2] = dblMat[i, 0] * dblMat[i, 0] + dblMat[i, 1] * dblMat[i, 1];
                dblMat[i, 3] = 1;
            }
            TestMat.Elements = (double[,])dblMat.Clone();
            if (TestMat.Determinant() > dblSmall)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Triangle Copy()
        //Creates a copy of the triangle to avoid reference problems.
        {
            Triangle TriCopy = new Triangle();
            TriCopy.FirstEdge = FirstEdge;
            TriCopy.ThirdPt = ThirdPt;
            TriCopy.Mark = Mark;
            return TriCopy;
        }

        public bool PtInTri(Point2D[] PtList, Edge[] Edgelist, Point2D TestPoint)
        //Returns true if the test point is inside the triangle.
        {
            bool flInTri=true;
            int iEdge=FirstEdge;
            double dblS = 0;
            double dblSmall = 1e-6;
            double dblA = PtList[ThirdPt].X * (PtList[Edgelist[iEdge].P1].Y - PtList[Edgelist[iEdge].P2].Y) +
                                PtList[ThirdPt].Y * (PtList[Edgelist[iEdge].P2].X - PtList[Edgelist[iEdge].P1].X) +
                                (PtList[Edgelist[iEdge].P1].X * PtList[Edgelist[iEdge].P2].Y -
                                PtList[Edgelist[iEdge].P2].X * PtList[Edgelist[iEdge].P1].Y);
            for (int i = 0; i < 3; i++)
            {
                dblS = TestPoint.X * (PtList[Edgelist[iEdge].P1].Y - PtList[Edgelist[iEdge].P2].Y) +
                                TestPoint.Y * (PtList[Edgelist[iEdge].P2].X - PtList[Edgelist[iEdge].P1].X) +
                                (PtList[Edgelist[iEdge].P1].X * PtList[Edgelist[iEdge].P2].Y -
                                PtList[Edgelist[iEdge].P2].X * PtList[Edgelist[iEdge].P1].Y);
                //BJG 12/12/14
                //Added dblsmall instead of zero.
                //Also changed it to be check of ratio of areas, rather than absolute:
                if (dblS/dblA < -dblSmall)
                {
                    flInTri = false;
                    break;
                }
                iEdge = Edgelist[iEdge].NextEdge;
            }
            return flInTri;
        }
    }
}
