using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace AeroTool
{
    public class Triangulation
    {

        //public Triangulation(){LGR.CreateLog("Mapomatic-Triangulation");}

        public void Delaunay2D(Point2D[] PtList, ref Triangle[] TriList, ref Edge[] EdgeList,
                                float ARMax)
        //Performs delaunay triangulation of the point list contained in PtList.
        {
            TriList = new Triangle[1];
            TriList[0] = new Triangle();
            EdgeList = new Edge[3];
            for (int i = 0; i < 3; i++)
            {
                EdgeList[i] = new Edge();
            }
            double small = 1e-6;
            NxNMatrix TestMat = new NxNMatrix(4);
            double[,] dblMat = new double[0, 0];
            int NPts = PtList.GetLength(0);
            int[] PtOrder = new int[NPts];
            //Start by creating a sigle triangle with positive order.
            EdgeList[0].P1 = 0;
            EdgeList[0].P2 = 1;
            PtList[0].flTriangulated = true;
            PtList[1].flTriangulated = true;
            TriList[0].FirstEdge = 0;
            //Find third point that gives non-zero area:
            TriList[0].ThirdPt = 1;
            do
            {
                TriList[0].ThirdPt++;
                if (TriList[0].ThirdPt >= NPts)
                {
                    return;
                }
            } while (Math.Abs(TriList[0].TriArea(PtList, EdgeList)) < small);
            PtList[TriList[0].ThirdPt].flTriangulated = true;
            //Correct order of points if necessary:
            if (TriList[0].TriArea(PtList, EdgeList) < 0)
            {
                EdgeList[0].P1 = 1;
                EdgeList[0].P2 = 0;
            }
            //Create two other edges:
            EdgeList[0].NextEdge = 1;
            EdgeList[0].Tri = 0;
            EdgeList[1].P1 = EdgeList[0].P2;
            EdgeList[1].P2 = TriList[0].ThirdPt;
            EdgeList[1].NextEdge = 2;
            EdgeList[1].Tri = 0;
            EdgeList[2].P1 = EdgeList[1].P2;
            EdgeList[2].P2 = EdgeList[0].P1;
            EdgeList[2].NextEdge = 0;
            EdgeList[2].Tri = 0;
            //Loop through rest of points, adding to the triangulation:
            Triangle TestTri = new Triangle();
            int NOldEdges = 0;
            int NOldTris = 0;
            int NNewEdges = 0;
            int NNewTris = 0;
            for (int i = 2; i < NPts; i++)
            {
                if (PtList[i].flTriangulated)
                {
                    continue;
                }
                //Need to count number of edges which are "visible" from new point:
                int NVisible = 0;
                TestTri.ThirdPt = i;
                for (int j = 0; j < EdgeList.GetLength(0); j++)
                {
                    if (EdgeList[j].OppEdge < 0)
                    {
                        TestTri.FirstEdge = j;
                        if (TestTri.TriArea(PtList, EdgeList) < -small)
                        {
                            EdgeList[j].Visible = true;
                            NVisible++;
                        }
                    }
                }
                /*
                 * If no  visible edges then point is inside convex hull. Mark triangles which have new point 
                 * inside their circumcircles:
                 */
                if (NVisible == 0)
                {
                    for (int j = 0; j < TriList.GetLength(0); j++)
                    {
                        if (TriList[j].PtInCircumcircle(PtList, EdgeList, i))
                        {
                            TriList[j].Mark = true;
                            //Mark tri's edges as visible:
                            int iEdge = TriList[j].FirstEdge;
                            EdgeList[iEdge].Visible = true;
                            iEdge = EdgeList[iEdge].NextEdge;
                            EdgeList[iEdge].Visible = true;
                            iEdge = EdgeList[iEdge].NextEdge;
                            EdgeList[iEdge].Visible = true;
                        }
                    }
                    //Mark edges whose opposite edges are also visible:
                    for (int j = 0; j < EdgeList.GetLength(0); j++)
                    {
                        if ((EdgeList[j].Visible) && (EdgeList[j].OppEdge >= 0))
                        {
                            if (EdgeList[EdgeList[j].OppEdge].Visible)
                            {
                                EdgeList[j].Mark = true;
                            }
                        }
                    }
                    //Delete marked triangles and edges:
                    DelMarkedEdges(ref TriList, ref EdgeList);
                    DelMarkedTris(ref TriList, ref EdgeList);
                }
                else
                /*
                 * This is the case where the new point is outside the convex hull of the point set.
                 */
                {
                    //Firstly, create opposite edge for each visible edge:
                    NOldEdges = EdgeList.GetLength(0);
                    NNewEdges = 0;
                    for (int j = 0; j < NOldEdges; j++)
                    {
                        if (EdgeList[j].Visible)
                        {
                            EdgeList[j].Visible = false;
                            EdgeList[j].OppEdge = NOldEdges + NNewEdges;
                            Edge NewEdge = new Edge();
                            NewEdge.P1 = EdgeList[j].P2;
                            NewEdge.P2 = EdgeList[j].P1;
                            NewEdge.OppEdge = j;
                            NewEdge.Visible = true;
                            AddEdge(ref EdgeList, NewEdge);
                            NNewEdges++;
                        }
                    }
                    //Check all triangles to see if the new point is inside their circumcircles:
                    NOldTris = TriList.GetLength(0);
                    for (int j = 0; j < NOldTris; j++)
                    {
                        if (TriList[j].PtInCircumcircle(PtList, EdgeList, i))
                        {
                            TriList[j].Mark = true;
                            //For each edge of this triangle, if area made with new point is negative, delete it,
                            //Otherwise mark it as visible:
                            int iEdge = TriList[j].FirstEdge;
                            for (int k = 0; k < 3; k++)
                            {
                                if (EdgeList[iEdge].TriAreaPositive(PtList, i))
                                {
                                    EdgeList[iEdge].Visible = true;
                                }
                                else
                                {
                                    EdgeList[iEdge].Mark = true;
                                }
                                iEdge = EdgeList[iEdge].NextEdge;
                            }
                        }
                    }
                    //Now mark all visible edges who's opposite edges are marked:
                    NOldEdges = EdgeList.GetLength(0);
                    for (int j = 0; j < NOldEdges; j++)
                    {
                        if ((EdgeList[j].Visible) && (EdgeList[j].OppEdge >= 0))
                        {
                            if (EdgeList[EdgeList[j].OppEdge].Mark)
                            {
                                EdgeList[j].Mark = true;
                            }
                        }
                    }
                    //Delete marked edges and triangles:
                    DelMarkedEdges(ref TriList, ref EdgeList);
                    DelMarkedTris(ref TriList, ref EdgeList);
                }
                NOldTris = TriList.GetLength(0);
                NOldEdges = EdgeList.GetLength(0);
                NNewTris = 0;
                NNewEdges = 0;
                //Add new triangles made up of remaining visible edges, and the new point:
                for (int j = 0; j < NOldEdges; j++)
                {
                    if (EdgeList[j].Visible)
                    {
                        //Create new triangle:
                        Triangle NewTri = new Triangle();
                        NewTri.FirstEdge = j;
                        NewTri.ThirdPt = i;
                        //Add to list:
                        AddTri(ref TriList, NewTri);
                        //Update original edge:
                        EdgeList[j].Visible = false;
                        EdgeList[j].NextEdge = NOldEdges + NNewEdges;
                        EdgeList[j].Tri = NOldTris + NNewTris;
                        //Create first of new edges:
                        Edge NewEdge = new Edge();
                        NewEdge.P1 = EdgeList[j].P2;
                        NewEdge.P2 = i;
                        NNewEdges++;
                        NewEdge.NextEdge = NOldEdges + NNewEdges;
                        NewEdge.Tri = NOldTris + NNewTris;
                        //Add to list:
                        AddEdge(ref EdgeList, NewEdge);
                        //Create second of new edges:
                        NewEdge.P1 = i;
                        NewEdge.P2 = EdgeList[j].P1;
                        NNewEdges++;
                        NewEdge.NextEdge = j;
                        NewEdge.Tri = NOldTris + NNewTris;
                        //Add to list:
                        AddEdge(ref EdgeList, NewEdge);
                        NNewTris++;
                    }
                }
                /*
                 * Update opposite edge of new edges - the opposite edge of each new edge will be
                 * one of the new edges, so it is only necessary to check the new edges.
                 */
                for (int j = NOldEdges; j < NOldEdges + NNewEdges; j++)
                {
                    for (int k = NOldEdges; k < NOldEdges + NNewEdges; k++)
                    {
                        if ((EdgeList[j].P1 == EdgeList[k].P2) && (EdgeList[j].P2 == EdgeList[k].P1))
                        {
                            EdgeList[j].OppEdge = k;
                            continue;
                        }
                    }
                    if (EdgeList[j].OppEdge > 0)
                    {
                        continue;
                    }
                }
                PtList[i].flTriangulated = true;
            }
            //Restore traingulated flag of all pts:
            for (int i = 0; i < NPts; i++)
            {
                PtList[i].flTriangulated = false;
            }
            /*
             * Go through triangles which are on the boundary and make sure that the aspect ratio is less than the
             * designated maximum.  If not, delete it and its edges:
             */
            int NTris = TriList.GetLength(0);
            //We need to do this loop maybe more than once to get rid of all slivers:
            int NChanges = 1;
            while (NChanges > 0)
            {
                NChanges = 0;
                NTris = TriList.GetLength(0);
                for (int i = 0; i < NTris; i++)
                {
                    int iEdge = TriList[i].FirstEdge;
                    bool flBdry = false;
                    for (int j = 0; j < 3; j++)
                    {
                        if (EdgeList[iEdge].OppEdge < 0)
                        {
                            flBdry = true;
                            break;
                        }
                        iEdge = EdgeList[iEdge].NextEdge;
                    }
                    if (flBdry)
                    {
                        //double dblLen = 0;
                        //dblLen = Math.Sqrt((PtList[EdgeList[iEdge].P2].X - PtList[EdgeList[iEdge].P1].X) *
                        //                (PtList[EdgeList[iEdge].P2].X - PtList[EdgeList[iEdge].P1].X) +
                        //                (PtList[EdgeList[iEdge].P2].Y - PtList[EdgeList[iEdge].P1].Y) *
                        //                (PtList[EdgeList[iEdge].P2].Y - PtList[EdgeList[iEdge].P1].Y));
                        //double dblTriArea = TriList[i].TriArea(PtList, EdgeList);
                        double dblAR = TriList[i].AR(PtList, EdgeList);
                        if (dblAR > ARMax)
                        //Mark tri and edges for deletion:
                        {
                            NChanges++;
                            TriList[i].Mark = true;
                            EdgeList[iEdge].Mark = true;
                            iEdge = EdgeList[iEdge].NextEdge;
                            EdgeList[iEdge].Mark = true;
                            iEdge = EdgeList[iEdge].NextEdge;
                            EdgeList[iEdge].Mark = true;
                        }
                    }
                }

                //Delete marked tris and edges:
                DelMarkedEdges(ref TriList, ref EdgeList);
                DelMarkedTris(ref TriList, ref EdgeList);
            }
            return;
        }

        public void DelMarkedTris(ref Triangle[] TriList, ref Edge[] EdgeList)
        //Delete marked triangles from triangle list:
        {
            Triangle[] TriListCopy = (Triangle[])TriList.Clone();
            int NOldTris = TriList.GetLength(0);
            //Count tris to remove:
            int NMarkedTris = 0;
            int NEdges = EdgeList.GetLength(0);
            for (int i = 0; i < NOldTris; i++)
            {
                if (TriList[i].Mark)
                {
                    NMarkedTris++;
                }
            }
            TriList = new Triangle[NOldTris - NMarkedTris];
            int NNewTris = 0;
            NMarkedTris=0;
            for (int i = 0; i < NOldTris; i++)
            {
                if (TriListCopy[i].Mark)
                {
                    //Need to remove all references to this triangle, and shuffle higher indices back by 1:
                    for (int j = 0; j < NEdges; j++)
                    {
                        if (EdgeList[j].Tri == i-NMarkedTris)
                        {
                            EdgeList[j].Tri = -1;
                        }
                        else if (EdgeList[j].Tri> i-NMarkedTris)
                        {
                            EdgeList[j].Tri--;
                        }
                    }
                    NMarkedTris++;
                }
            }
            for (int i = 0; i < NOldTris; i++)
            {
                if (!TriListCopy[i].Mark)
                {
                    TriList[NNewTris] = TriListCopy[i];
                    NNewTris++;
                }
            }
            return;
        }

        public void DelMarkedEdges(ref Triangle[] TriList, ref Edge[] EdgeList)
        //Delete marked edges from edge list:
        {
            Edge[] EdgeListCopy = (Edge[]) EdgeList.Clone();
            int NOldEdges = EdgeList.GetLength(0);
            //Count tris to remove:
            int NMarkedEdges = 0;
            int NTris = TriList.GetLength(0);
            for (int i = 0; i < NOldEdges; i++)
            {
                if (EdgeList[i].Mark)
                {
                    NMarkedEdges++;
                }
            }
            EdgeList = new Edge[NOldEdges - NMarkedEdges];
            int NNewEdges = 0;
            NMarkedEdges=0;
            for (int i = 0; i < NOldEdges; i++)
            {
                if (EdgeListCopy[i].Mark)
                {
                    //Need to remove all references to this edge, and shuffle higher indices back by 1:
                    for (int j = 0; j < NTris; j++)
                    {
                        if (TriList[j].FirstEdge == i-NMarkedEdges)
                        {
                            TriList[j].FirstEdge = -1;
                        }
                        else if (TriList[j].FirstEdge > i-NMarkedEdges)
                        {
                            TriList[j].FirstEdge--;
                        }
                    }
                    //There also may be reference to this edge in the edgelist (as nextedge or oppedge):
                    for (int j = 0; j < NOldEdges; j++)
                    {
                        if (EdgeListCopy[j].NextEdge == i - NMarkedEdges)
                        {
                            EdgeListCopy[j].NextEdge = -1;
                        }
                        else if (EdgeListCopy[j].NextEdge > i - NMarkedEdges)
                        {
                            EdgeListCopy[j].NextEdge--;
                        }
                        if (EdgeListCopy[j].OppEdge == i - NMarkedEdges)
                        {
                            EdgeListCopy[j].OppEdge = -1;
                        }
                        else if (EdgeListCopy[j].OppEdge > i - NMarkedEdges)
                        {
                            EdgeListCopy[j].OppEdge--;
                        }
                    }
                    NMarkedEdges++;
                }
            }
            for (int i=0;i<NOldEdges;i++)
            {
                if (!EdgeListCopy[i].Mark)
                {
                    EdgeList[NNewEdges] = EdgeListCopy[i];
                    NNewEdges++;
                }
            }
            return;
        }

        public void AddTri(ref Triangle[] TriList, Triangle NewTri)
        //Add single triangle to list:
        {
            Triangle[] TriListCopy = (Triangle[])TriList.Clone();
            int NOldTris = TriList.GetLength(0);
            TriList = new Triangle[NOldTris + 1];
            for (int i = 0; i < NOldTris; i++)
            {
                TriList[i] = TriListCopy[i];
            }
            TriList[NOldTris] = NewTri.Copy();
        }

        public void AddEdge(ref Edge[] EdgeList, Edge NewEdge)
        //Add singe edge to list.
        {
            Edge[] EdgeListCopy = (Edge[])EdgeList.Clone();
            int NOldEdges = EdgeList.GetLength(0);
            EdgeList = new Edge[NOldEdges + 1];
            for (int i = 0; i < NOldEdges; i++)
            {
                EdgeList[i] = EdgeListCopy[i];
            }
            EdgeList[NOldEdges] = NewEdge.Copy();
        }

        public double[,] TriPlaneCoeffs(Triangle[] TriList, Edge[] EdgeList, Point2D[] PtList)
        //Calculates coefficients of plane fit through each triangle of points.
        {
            int NTris = TriList.GetLength(0);
            double[] Values = new double[3];
            NxNMatrix TriMat = new NxNMatrix(3);
            double[,] TriCoeffs = new double[NTris, 3];
            for (int i = 0; i < NTris; i++)
            {
                TriMat.Elements[0, 0] = PtList[EdgeList[TriList[i].FirstEdge].P1].X;
                TriMat.Elements[1, 0] = PtList[EdgeList[TriList[i].FirstEdge].P1].Y;
                Values[0] = PtList[EdgeList[TriList[i].FirstEdge].P1].Value;
                TriMat.Elements[0, 1] = PtList[EdgeList[TriList[i].FirstEdge].P2].X;
                TriMat.Elements[1, 1] = PtList[EdgeList[TriList[i].FirstEdge].P2].Y;
                Values[1] = PtList[EdgeList[TriList[i].FirstEdge].P2].Value;
                TriMat.Elements[0, 2] = PtList[TriList[i].ThirdPt].X;
                TriMat.Elements[1, 2] = PtList[TriList[i].ThirdPt].Y;
                Values[2] = PtList[TriList[i].ThirdPt].Value;
                for (int j = 0; j < 3; j++)
                {
                    TriMat.Elements[2, j] = 1;
                }
                NxNMatrix InvMat = TriMat.Inverse();
                for (int j = 0; j < 3; j++)
                {
                    TriCoeffs[i, j] = 0;
                    for (int k = 0; k < 3; k++)
                    {
                        TriCoeffs[i, j] += Values[k] * InvMat.Elements[k, j];
                    }
                }
            }
            return TriCoeffs;
        }

        public int[] ConvHull(Edge[] Edgelist)
        //Returns list of indices of edges, in order, which make up the boundary of the triangulation.
        {
            int NEdges=Edgelist.GetLength(0);       //Number of edges in list.
            int NHullEdges=0;                       //Number of edges on boundary.
            int iFirstEdge=0;                       //Index of first edge.
            //Count number of edges without an oppposite edge, i.e. that dont back onto another triangle.
            for (int i=0;i<NEdges;i++)
            {
                if (Edgelist[i].OppEdge<0)
                {
                    if (NHullEdges==0)
                    {
                        iFirstEdge=i;
                    }
                    NHullEdges++;
                }
            }
            int[] iConvHull=new int[NHullEdges];
            iConvHull[0] = iFirstEdge;
            NHullEdges=0;
            //Find edgeswhose first point is equal to current edge's second point, i.e. next point around hull:
            do
            {
                for (int i = 0; i < NEdges; i++)
                {
                    if ((Edgelist[i].OppEdge < 0) && (Edgelist[i].P1 == Edgelist[iConvHull[NHullEdges]].P2))
                    {
                        NHullEdges++;
                        iConvHull[NHullEdges] = i;
                    }
                }
            } while (Edgelist[iConvHull[NHullEdges]].P2 != Edgelist[iConvHull[0]].P1);
            return iConvHull;
        }

        public double[,] ConvHullNormals(Point2D[] PtList, Edge[] EdgeList, int[] iConvHull, Triangle[] TriList)
        //Returns normals to each convex hull edge, together with the plane fit gradient from the local triangle
        //resolved in the direction normal to that edge.
        {
            int NConvEdges = iConvHull.GetLength(0);    //Number of edges on convex hull.
            double[,] dHullNormals = new double[NConvEdges, 2];
            double[] UnitNorm = new double[2];
            for (int i = 0; i < NConvEdges; i++)
            {
                UnitNorm = EdgeList[iConvHull[i]].UnitNormal(PtList);
                dHullNormals[i, 0] = UnitNorm[0];
                dHullNormals[i, 1] = UnitNorm[1];
            }
            return dHullNormals;
        }

        public Point2D[] AvConvHullNormals(Edge[] EdgeList, int[] iCHull, double[,] dHullNormals, double[,] TriCoeffs)
        /* 
         * Calculates average normal unit vectors at each vertex around convex hull, together with the planar gradient 
         * values in the direction of those vectors.
         */
        {
            int NCHull = iCHull.GetLength(0);
            Point2D[] AvNormals = new Point2D[NCHull];
            //Calculate average normals:
            for (int i = 0; i < NCHull; i++)
            {
                AvNormals[i] = new Point2D();
                if (i == 0)
                {
                    AvNormals[i].X = (dHullNormals[NCHull - 1, 0] + dHullNormals[0, 0]) / 2f;
                    AvNormals[i].Y = (dHullNormals[NCHull - 1, 1] + dHullNormals[0, 1]) / 2f;
                }
                else
                {
                    AvNormals[i].X = (dHullNormals[i - 1, 0] + dHullNormals[i, 0]) / 2f;
                    AvNormals[i].Y = (dHullNormals[i - 1, 1] + dHullNormals[i, 1]) / 2f;
                }
                //need to renormalise:
                double Mag1 = Math.Sqrt(AvNormals[i].X * AvNormals[i].X + AvNormals[i].Y * AvNormals[i].Y);
                AvNormals[i].X /= Mag1;
                AvNormals[i].Y /= Mag1;
                //Calculate gradients by average of dot products of normals with planar gradients in 
                //adjacent triangles:
                int Tri1;
                int Tri2;
                if (i == 0)
                {
                    Tri1 = EdgeList[iCHull[NCHull - 1]].Tri;
                    Tri2 = EdgeList[iCHull[i]].Tri;
                }
                else
                {
                    Tri1 = EdgeList[iCHull[i - 1]].Tri;
                    Tri2 = EdgeList[iCHull[i]].Tri;
                }
                AvNormals[i].Value = AvNormals[i].X * (TriCoeffs[Tri1, 0] + TriCoeffs[Tri2, 0]) / 2f +
                            AvNormals[i].Y * (TriCoeffs[Tri1, 1] + TriCoeffs[Tri2, 1]) / 2f;
            }
            return AvNormals;
        }

        public Point2D Intersect(Point2D P1, Point2D Vec1, Point2D P2, Point2D Vec2)
        //Case where s is not required:
        {
            double s = 0;
            return Intersect(P1, Vec1, P2, Vec2, ref s);
        }
        
        public Point2D Intersect(Point2D P1, Point2D Vec1, Point2D P2, Point2D Vec2, ref double s)
        /*
         * Calculates the intersection between two infinite lines, each definied by one point, and a 
         * direction vector.  Vec1 must be a unit direction vector, with the gradient contained within it as the value
         * to return the correct value at the intersect point.
         */
        {
            double a0 = P1.X;
            double a1 = P1.Y;
            double A0 = Vec1.X;
            double A1 = Vec1.Y;
            double b0 = P2.X;
            double b1 = P2.Y;
            double B0 = Vec2.X;
            double B1 = Vec2.Y;
            Point2D IntersecPt = new Point2D();
            double Denom = A1 * B0 - A0 * B1;
            if (Denom == 0)
            {
                //In this case the lines are parallel:
                IntersecPt.flError = true;
                return IntersecPt;
            }
            s = (B0 * (b1 - a1) + B1 * (a0 - b0))/Denom;
            IntersecPt.X = P1.X + s * Vec1.X;
            IntersecPt.Y = P1.Y + s * Vec1.Y;
            IntersecPt.Value = P1.Value + s * Vec1.Value;
            return IntersecPt;
        }

        public void InterpVal(ref Point2D TestPoint, Point2D[] PtList, Edge[] EdgeList, Triangle[] TriList,
                               int NTris, double[,] TriCoeffs, int[] iCHull, int NCHull, Point2D[] AvHullNormals, 
                                ref bool flOutsideHull, bool flExtrapolate)
        /*
         * Function returns the value of the interpolation/extrapolation at the point contained in TestPt, given a
         * list of points, and the delaunay triangulation of those points, and the normals to the boundary.
         * The boolean value flExtrap is True if the returned value is extrapolated outside the triangulation, and 
         * false if it is interpolated from within the triangulation.
         */
        {
            int iTri = -1;
            for (int k = 0; k < NTris; k++)
            {
                if (TriList[k].PtInTri(PtList, EdgeList, TestPoint))
                {
                    iTri = k;
                    break;
                }
            }
            double dblMatTest = 0;
            if (iTri < 0)
            /*
             * Extrapolation works by a bilinear interpolation between the average normal vectors at each 
             * vertex around the boundary, or convex hull, of the dataset.
             * */
            {
                //Value is extrapolated, so set flag:
                flOutsideHull = true;
                //Find chull edge to use for extrapolation.
                int iEdge = -1;
                double[] pVec1 = new double[2];
                double dblSmall = 1e-6;
                for (int k = 0; k < NCHull; k++)
                {
                    bool flToLeft = true;
                    NxNMatrix TestMat = new NxNMatrix(3);
                    //We need to make sure that the test point is "to the left" of the 
                    //hull edge, and both normals from either end:
                    //Test first normal:
                    TestMat.Elements[0, 0] = PtList[EdgeList[iCHull[k]].P1].X;
                    TestMat.Elements[0, 1] = PtList[EdgeList[iCHull[k]].P1].Y;
                    TestMat.Elements[1, 0] = TestMat.Elements[0, 0] + AvHullNormals[k].X;
                    TestMat.Elements[1, 1] = TestMat.Elements[0, 1] + AvHullNormals[k].Y;
                    TestMat.Elements[2, 0] = TestPoint.X;
                    TestMat.Elements[2, 1] = TestPoint.Y;
                    for (int l = 0; l < 3; l++)
                    {
                        TestMat.Elements[l, 2] = 1f;
                    }
                    dblMatTest = TestMat.Determinant();
                    //Now have changed to allow more tolerance on triangle, so should have been picked up as being
                    //in a triangle before we get to this point:
                    if (dblMatTest < 0)
                    {
                        flToLeft = false;
                    }
                    else
                    {
                        TestMat.Elements[1, 0] = PtList[EdgeList[iCHull[k]].P2].X;
                        TestMat.Elements[1, 1] = PtList[EdgeList[iCHull[k]].P2].Y;
                        if (k == NCHull - 1)
                        {
                            TestMat.Elements[0, 0] = TestMat.Elements[1, 0] + AvHullNormals[0].X;
                            TestMat.Elements[0, 1] = TestMat.Elements[1, 1] + AvHullNormals[0].Y;
                        }
                        else
                        {
                            TestMat.Elements[0, 0] = TestMat.Elements[1, 0] + AvHullNormals[k + 1].X;
                            TestMat.Elements[0, 1] = TestMat.Elements[1, 1] + AvHullNormals[k + 1].Y;
                        }
                        dblMatTest = TestMat.Determinant();
                        //Have to leave small in here..  Not sure what to do about that.
                        if (dblMatTest < -dblSmall)
                        {
                            flToLeft = false;
                        }
                        else
                        //Test edge itself:
                        {
                            TestMat.Elements[0, 0] = PtList[EdgeList[iCHull[k]].P2].X;
                            TestMat.Elements[0, 1] = PtList[EdgeList[iCHull[k]].P2].Y;
                            TestMat.Elements[1, 0] = PtList[EdgeList[iCHull[k]].P1].X;
                            TestMat.Elements[1, 1] = PtList[EdgeList[iCHull[k]].P1].Y;
                            dblMatTest = TestMat.Determinant();
                            if (dblMatTest < -dblSmall)
                            {
                                flToLeft = false;
                            }
                        }
                    }
                    if (flToLeft)
                    {
                        iEdge = k;
                        break;
                    }
                }

                //Calculate intercepts of point and line parallel to hull edge with these normals:
                //******************************************************************************
                
                Point2D P1 = PtList[EdgeList[iCHull[iEdge]].P1].Copy();
                Point2D P2 = PtList[EdgeList[iCHull[iEdge]].P2].Copy();
                Point2D P1Intersec = new Point2D();
                Point2D P2Intersec = new Point2D();
                Point2D EdgeVec = new Point2D();

                EdgeVec.X = PtList[EdgeList[iCHull[iEdge]].P2].X - PtList[EdgeList[iCHull[iEdge]].P1].X;
                EdgeVec.Y = PtList[EdgeList[iCHull[iEdge]].P2].Y - PtList[EdgeList[iCHull[iEdge]].P1].Y;


                P1Intersec = Intersect(P1, AvHullNormals[iEdge], TestPoint, EdgeVec);
                if (!flExtrapolate)
                {
                    //Force value of this point to be that of P2, i.e. no extrapolation:
                    P1Intersec.Value = P1.Value;
                }


                if (iEdge < NCHull - 1)
                {
                    P2Intersec = Intersect(P2, AvHullNormals[iEdge + 1], TestPoint, EdgeVec);
                }
                else
                {
                    P2Intersec = Intersect(P2, AvHullNormals[0], TestPoint, EdgeVec);
                }

                if (!flExtrapolate)
                {
                    //Force value of this point to be that of P2, i.e. no extrapolation:
                    P2Intersec.Value = P2.Value;
                }
                //We now perform a linear interpolation between the two:
                double DistP12 = Math.Sqrt((P2Intersec.X - P1Intersec.X) * (P2Intersec.X - P1Intersec.X) +
                                            (P2Intersec.Y - P1Intersec.Y) * (P2Intersec.Y - P1Intersec.Y));
                double DistP1X = Math.Sqrt((TestPoint.X - P1Intersec.X) * (TestPoint.X - P1Intersec.X) +
                                            (TestPoint.Y - P1Intersec.Y) * (TestPoint.Y - P1Intersec.Y));
                TestPoint.Value = P1Intersec.Value + (P2Intersec.Value - P1Intersec.Value) * DistP1X / DistP12;
            }
            else
            //Else interpolate value:
            {
                //Interpolated, so set flExtrap to false:
                flOutsideHull = false;
                TestPoint.Value = TestPoint.X * TriCoeffs[iTri, 0] +
                            TestPoint.Y * TriCoeffs[iTri, 1] +
                            TriCoeffs[iTri, 2];
            }
        }

        public double[] WeightVals(Point2D TestPoint, Point2D[] PtList, Edge[] EdgeList, Triangle[] TriList,
                               int NTris, int[] iCHull, int NCHull, Point2D[] AvHullNormals)
        /*
         * Given a test point, the function returns the weights for each point which are required for each of the grid points
         * which multiplied by the value at each point will give the interpolated value at the test point.
         * I am going to use this function to calculate wind tunnel weights from measured heights at the track.
         * Alogrithm is practically identical as the interpolation one.
         */
        {
            int iTri = -1;
            for (int k = 0; k < NTris; k++)
            {
                if (TriList[k].PtInTri(PtList, EdgeList, TestPoint))
                {
                    iTri = k;
                    break;
                }
            }
            double dblMatTest = 0;
            int NPts = PtList.GetLength(0);
            double[] dblWeights = new double[NPts];
            //Initialise weights array:
            for (int i = 0; i < NPts; i++)
            {
                dblWeights[i] = 0.0;
            }
            if (iTri < 0)
            /*
             * Extrapolation works by a bilinear interpolation between the average normal vectors at each 
             * vertex around the boundary, or convex hull, of the dataset.
             * */
            {
                //Find chull edge to use for extrapolation.
                int iEdge = -1;
                double[] pVec1 = new double[2];
                double dblSmall = 1e-6;
                for (int k = 0; k < NCHull; k++)
                {
                    bool flToLeft = true;
                    NxNMatrix TestMat = new NxNMatrix(3);
                    //We need to make sure that the test point is "to the left" of the 
                    //hull edge, and both normals from either end:
                    //Test first normal:
                    TestMat.Elements[0, 0] = PtList[EdgeList[iCHull[k]].P1].X;
                    TestMat.Elements[0, 1] = PtList[EdgeList[iCHull[k]].P1].Y;
                    TestMat.Elements[1, 0] = TestMat.Elements[0, 0] + AvHullNormals[k].X;
                    TestMat.Elements[1, 1] = TestMat.Elements[0, 1] + AvHullNormals[k].Y;
                    TestMat.Elements[2, 0] = TestPoint.X;
                    TestMat.Elements[2, 1] = TestPoint.Y;
                    for (int l = 0; l < 3; l++)
                    {
                        TestMat.Elements[l, 2] = 1f;
                    }
                    dblMatTest = TestMat.Determinant();
                    if (dblMatTest < -dblSmall)
                    {
                        flToLeft = false;
                    }
                    else
                    {
                        TestMat.Elements[1, 0] = PtList[EdgeList[iCHull[k]].P2].X;
                        TestMat.Elements[1, 1] = PtList[EdgeList[iCHull[k]].P2].Y;
                        if (k == NCHull - 1)
                        {
                            TestMat.Elements[0, 0] = TestMat.Elements[1, 0] + AvHullNormals[0].X;
                            TestMat.Elements[0, 1] = TestMat.Elements[1, 1] + AvHullNormals[0].Y;
                        }
                        else
                        {
                            TestMat.Elements[0, 0] = TestMat.Elements[1, 0] + AvHullNormals[k + 1].X;
                            TestMat.Elements[0, 1] = TestMat.Elements[1, 1] + AvHullNormals[k + 1].Y;
                        }
                        dblMatTest = TestMat.Determinant();
                        if (dblMatTest < -dblSmall)
                        {
                            flToLeft = false;
                        }
                        else
                        //Test edge itself:
                        {
                            TestMat.Elements[0, 0] = PtList[EdgeList[iCHull[k]].P2].X;
                            TestMat.Elements[0, 1] = PtList[EdgeList[iCHull[k]].P2].Y;
                            TestMat.Elements[1, 0] = PtList[EdgeList[iCHull[k]].P1].X;
                            TestMat.Elements[1, 1] = PtList[EdgeList[iCHull[k]].P1].Y;
                            dblMatTest = TestMat.Determinant();
                            if (dblMatTest < -dblSmall)
                            {
                                flToLeft = false;
                            }
                        }
                    }
                    if (flToLeft)
                    {
                        iEdge = k;
                        break;
                    }
                }
                //Calculate intercepts of point and line parallel to hull edge with these normals:
                //******************************************************************************************

                Point2D P1 = PtList[EdgeList[iCHull[iEdge]].P1].Copy();
                Point2D P2 = PtList[EdgeList[iCHull[iEdge]].P2].Copy();
                Point2D P1Intersec = new Point2D();
                Point2D P2Intersec = new Point2D();
                Point2D EdgeVec = new Point2D();
                double[] NormVec = new double[2];
                Point2D PtNormVec = new Point2D();
                double d = 0;
                double s = 0;
                //Get vector normal to the hull edge:
                NormVec = EdgeList[iCHull[iEdge]].UnitNormal(PtList);
                //We need this as a point though, so:
                PtNormVec.X = NormVec[0];
                PtNormVec.Y = NormVec[1];
                //Get hull edge as a point2d:
                EdgeVec.X = PtList[EdgeList[iCHull[iEdge]].P2].X - PtList[EdgeList[iCHull[iEdge]].P1].X;
                EdgeVec.Y = PtList[EdgeList[iCHull[iEdge]].P2].Y - PtList[EdgeList[iCHull[iEdge]].P1].Y;
                //Calculate intersection between a line normal to the hull edge through the test point, and the hull edge:
                P1Intersec = Intersect(P1, EdgeVec, TestPoint, PtNormVec);
                d = Math.Sqrt(EdgeVec.X * EdgeVec.X + EdgeVec.Y * EdgeVec.Y);
                EdgeVec.X = P1Intersec.X - PtList[EdgeList[iCHull[iEdge]].P1].X;
                EdgeVec.Y = P1Intersec.Y - PtList[EdgeList[iCHull[iEdge]].P1].Y;
                s = Math.Sqrt(EdgeVec.X * EdgeVec.X + EdgeVec.Y * EdgeVec.Y);
                if (s < 0)
                {
                    dblWeights[EdgeList[iCHull[iEdge]].P1] = 1;
                    dblWeights[EdgeList[iCHull[iEdge]].P2] = 0;
                }
                else if (s > d)
                {
                    dblWeights[EdgeList[iCHull[iEdge]].P1] = 0;
                    dblWeights[EdgeList[iCHull[iEdge]].P2] = 1;
                }
                else
                {
                    dblWeights[EdgeList[iCHull[iEdge]].P1] = s / d;
                    dblWeights[EdgeList[iCHull[iEdge]].P2] = 1 - s / d;
                }
            }
            else
            //Else interpolate value:
            {
                //Calculate weights from plane fit equation.
                NxNMatrix TriMat = new NxNMatrix(3);
                NxNMatrix InvMat = new NxNMatrix(3);
                TriMat.Elements[0, 0] = PtList[EdgeList[TriList[iTri].FirstEdge].P1].X;
                TriMat.Elements[1, 0] = PtList[EdgeList[TriList[iTri].FirstEdge].P1].Y;
                TriMat.Elements[2, 0] = 1;
                TriMat.Elements[0, 1] = PtList[EdgeList[TriList[iTri].FirstEdge].P2].X;
                TriMat.Elements[1, 1] = PtList[EdgeList[TriList[iTri].FirstEdge].P2].Y;
                TriMat.Elements[2, 1] = 1;
                TriMat.Elements[0, 2] = PtList[TriList[iTri].ThirdPt].X;
                TriMat.Elements[1, 2] = PtList[TriList[iTri].ThirdPt].Y;
                TriMat.Elements[2, 2] = 1;
                //Invert this matrix:
                InvMat = TriMat.Inverse();
                //Weights are a product of the inverse matrix and the point vector:
                dblWeights[EdgeList[TriList[iTri].FirstEdge].P1] = InvMat.Elements[0, 0] * TestPoint.X + InvMat.Elements[0, 1] * TestPoint.Y + InvMat.Elements[0, 2];
                dblWeights[EdgeList[TriList[iTri].FirstEdge].P2] = InvMat.Elements[1, 0] * TestPoint.X + InvMat.Elements[1, 1] * TestPoint.Y + InvMat.Elements[1, 2];
                dblWeights[TriList[iTri].ThirdPt] = InvMat.Elements[2, 0] * TestPoint.X + InvMat.Elements[2, 1] * TestPoint.Y + InvMat.Elements[2, 2];
            }
            return dblWeights;
        }
    }
}